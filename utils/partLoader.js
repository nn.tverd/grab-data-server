const csv = require("csvtojson");
const axios = require("axios");
const config = require("./configs/config.js");
// http://${config.serverIp}:${config.serverPort}


const csvFilePath = "./data-sources/export-parts.csv";
csv()
    .fromFile(csvFilePath)
    .then((jsonObj) => {
        console.log(jsonObj);
        for (let i in jsonObj) {
            const row = jsonObj[i];
            const Part = {
                dataBaseId: row._id,
                name: row.name,
                partNumber: row.partNumber,
                partSections: row.partSections,
                catalogs: row.catalogs,
                analogsGroup: row.analogsGroup,
            };
            axios({
                method: "post",
                url: `http://${config.serverIp}:${config.serverPort}/part`,
                data: Part,
                headers: {
                    "Content-Type": "application/json",
                },
            })
                .then((res) => {
                    console.log(res);

                    // dispatch({ type: UPDATE_ABSTRACT_PART_TO_SHOW, payload: { abstractPartToShow: abstractPartToShow, abstractPartToShowIds: abstractPartToShowIds } });
                })
                .catch((err) => {
                    console.log(err);
                });
        }
    });
