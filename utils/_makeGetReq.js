const url = require("url");
const http = require("http");
const HttpProxyAgent = require("http-proxy-agent");

const { getProxy } = require("./_getProxy.js");

async function makeGetReqAtempts(endpoint) {
    const startTime = new Date();
    for (let i = 0; i < 50; i++) {
        try {
            const result = await makeGetReq(endpoint);
            if (result.status == 200) {
                i = 1000;
                // console.log(result);
                const endTime = new Date();
                console.log(
                    "fetching takes, s:",
                    endTime.getTime() - startTime.getTime()
                );
                return result;
            }
        } catch (err) {
            console.log(err);
            // return err;
        }
    }
}

function makeGetReq(endpoint) {
    return new Promise((resolve, reject) => {
        // HTTP/HTTPS proxy to connect to
        // for (let i = 0; i < 50; i++) {
        const proxy = getProxy();
        console.log("using proxy server %j", proxy);

        // HTTP endpoint for the proxy to connect to
        console.log("attempting to GET %j", endpoint);
        var opts = url.parse(endpoint);

        // create an instance of the `HttpProxyAgent` class with the proxy server information
        var agent = new HttpProxyAgent(proxy);
        if (proxy !== "localhost") {
            opts.agent = agent;
        }

        const req = http.get(opts, (res) => {
            if (res.statusCode < 200 || res.statusCode >= 300) {
                return reject({
                    status: -200,
                    body: new Error("statusCode=" + res.statusCode),
                });
            }
            i = 1000;
            var body = [];
            res.on("data", function (chunk) {
                body.push(chunk);
            });

            res.on("end", function () {
                try {
                    body = Buffer.concat(body).toString();
                    // console.log(body);
                } catch (e) {
                    reject({ status: -200, body: e });
                }
                return resolve({ status: 200, body: body });
            });
        });
        req.on("error", (e) => {
            reject({ status: -200, body: e.message });
        });
        // send the request
        req.end();
        // return req;
        // }
    });
}
function makeGetReq2(endpoint, withProxy) {
    return new Promise((resolve, reject) => {
        // HTTP/HTTPS proxy to connect to
        const proxy = withProxy;
        console.log("using proxy server %j", proxy);

        // HTTP endpoint for the proxy to connect to
        console.log("attempting to GET %j", endpoint);
        var opts = url.parse(endpoint);

        // create an instance of the `HttpProxyAgent` class with the proxy server information
        var agent = new HttpProxyAgent(proxy);
        opts.agent = agent;

        const req = http.get(opts, (res) => {
            console.log(res.statusCode);
            if (res.statusCode < 200 || res.statusCode >= 300) {
                return reject(new Error("statusCode=" + res.statusCode));
            } else {
                resolve(proxy);
            }
            return;
            var body = [];
            res.on("data", function (chunk) {
                body.push(chunk);
            });

            res.on("end", function () {
                try {
                    body = Buffer.concat(body).toString();
                    // console.log(body);
                } catch (e) {
                    reject(e);
                }
                resolve(body);
            });
        });
        req.on("error", (e) => {
            reject(e.message);
        });
        // send the request
        req.end();
        // return req;
    });
}

module.exports.makeGetReq = makeGetReq;
module.exports.makeGetReq2 = makeGetReq2;
module.exports.makeGetReqAtempts = makeGetReqAtempts;

/*
httprequest().then((data) => {
        const response = {
            statusCode: 200,
            body: JSON.stringify(data),
        };
    return response;
});
function httprequest() {
     return new Promise((resolve, reject) => {
        const options = {
            host: 'jsonplaceholder.typicode.com',
            path: '/todos',
            port: 443,
            method: 'GET'
        };
        const req = http.request(options, (res) => {
          if (res.statusCode < 200 || res.statusCode >= 300) {
                return reject(new Error('statusCode=' + res.statusCode));
            }
            var body = [];
            res.on('data', function(chunk) {
                body.push(chunk);
            });
            res.on('end', function() {
                try {
                    body = JSON.parse(Buffer.concat(body).toString());
                } catch(e) {
                    reject(e);
                }
                resolve(body);
            });
        });
        req.on('error', (e) => {
          reject(e.message);
        });
        // send the request
       req.end();
    });
}


*/
