// if there is no search button to click we shoul skip this item
// 96243577
const puppeteer = require("puppeteer");
const fetch = require("node-fetch");
const axios = require("axios");
const cheerio = require("cheerio");
// const fetch = require("node-fetch-with-proxy");
const config = require("./configs/config.js");
// http://${config.serverIp}:${config.serverPort}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
const blockExec = false;
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
const params = {
    startUrl: "http://catalog.phaeton.kz",
    requestType: "parts class acquisition",
    requestTypeToSearch: "car modification acqusition additional information",
    treatedByAlgoriths:
        "phaeton-catalogs/phaetonTecDocDownloadDownloadPartsClasses.js",
};
//-------------------------------------------------------------------------------
async function getCarModifications() {
    try {
        const response = await axios({
            method: "post",
            url: `http://${config.serverIp}:${config.serverPort}/inputdocs/search`,
            data: {
                cond: {
                    requestType: params.requestTypeToSearch,
                },
                // limit: 100000,
                limit: 1,
            },
            headers: {
                "Content-Type": "application/json",
            },
        });
        console.log(response.data.length);
        return response.data;
        // return new Promise((resolve) => {
        //     resolve(response.data);
        // });
    } catch (err) {
        console.log(err);
        return [];
    }
}
//-------------------------------------------------------------------------------
// const sleep = require("util").promisify(setTimeout)(
function timeout(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

const start_url = "http://catalog.phaeton.kz";

async function onPuppeteer() {
    console.log("- - onPuppeteer - start .....");
    const carModifications = await getCarModifications();
    console.log(
        "- - onPuppeteer  .....",
        "carModifications.length",
        carModifications.length
    );

    const browser = await puppeteer.launch({
        headless: true,
        devtools: false,
        args: ["--no-sandbox"],
    });
    console.log(
        "- - onPuppeteer  .....",
        "const browser = await puppeteer.launch({"
    );
    const page = await browser.newPage();
    console.log(
        "- - onPuppeteer  .....",
        "const page = await browser.newPage();"
    );

    await page.setViewport({
        width: 1920,
        height: 1080,
    });
    console.log("- - onPuppeteer  .....", "await page.setViewport({");

    for (let i = 0; i < carModifications.length; i++) {
        try {
            const carModification = carModifications[i];

            await page.goto(start_url + carModification.payload.link, {
                waitUntil: "networkidle2",
            });
            console.log(
                "- - onPuppeteer  .....",
                "await page.goto(start_url + carModification.payload.link, {"
            );

            // break;
        } catch (err) {
            console.log("- - onPuppeteer  .....", "catch (err) {");

            console.log(i, "err", err);
            console.log(i);
            i -= 1;
            console.log(i);
            continue;
        }
    }
    console.log("- - onPuppeteer  .....", "test Ok");
    browser.close();
}
async function onFetch() {
    console.log("- - onFetch - start .....");

    const carModifications = await getCarModifications();
    console.log(
        "- - onFetch - start .....",
        "carModifications.length",
        carModifications.length
    );

    for (let i in carModifications) {
        const carModification = carModifications[i];
        try {
            const res = await fetch(
                start_url + carModification.payload.additionalInfoClick
            );
            const text = await res.text();
            console.log(
                "- - onFetch - start .....",
                "const res = await fetch( - text",
                text.length
            );
        } catch (err) {
            console.log("- - onFetch - start .....", "} catch (err) {");
            console.log(err);
        }

        // break;
    }
    console.log("- - onFetch  .....", "test Ok");
}

(async () => {
    if (blockExec) {
        console.log("blockExec");
        return 0;
    }
    console.log(`${params.treatedByAlgoriths}.....`);
    await onFetch();
    await onPuppeteer();
    // for(;;){
    //     console.log( "infinite loop" )
    // }
    
})();
