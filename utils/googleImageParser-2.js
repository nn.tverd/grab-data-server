// const fs = require("fs");
const axios = require("axios");
const config = require("./configs/config.js");
// http://${config.serverIp}:${config.serverPort}



// const getSessionCoolie = require("./getSessionCookie");
const { Builder, By, Key, until } = require("selenium-webdriver");

// const links = require("./credentials/links.json");
//-------------------------------------------------------------------------------
const getParts = () => {
    axios({
        method: "get",
        url: `http://${config.serverIp}:${config.serverPort}/part`,
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((res) => {
            // console.log(res);
            const parts = res.data;
            for (let i in parts) {
                const part = parts[i];
                const parsedData = parsePartNumbersList(part);
                break;
            }
            // return data;

            // dispatch({ type: UPDATE_ABSTRACT_PART_TO_SHOW, payload: { abstractPartToShow: abstractPartToShow, abstractPartToShowIds: abstractPartToShowIds } });
        })
        .catch((err) => {
            console.log(err);
            return [];
        });
    return [];
};
//-------------------------------------------------------------------------------
(async function main() {
    const parts = await getParts();
    console.log(parts);
})();
const link_base = "https://www.google.ru/search?tbm=isch&q=";
// const link_base = "https://www.google.ru/search?tbm=isch&q=GM+96169594";

async function parsePartNumbersList(part) {
    const link = `${link_base}${part.brand}+${part.partNumber}`;
    console.log(link);
    let driver = await new Builder().forBrowser("chrome").build();
    try {
        await driver.get(link);
        await driver.wait(until.elementLocated(By.id("islrg")), 10000);
        const blocks = await driver.findElements(By.xpath("//div[@jsaction]"));
        for (let i in blocks) {
            const block = blocks[i];
            const img = await block.findElement(By.xpath("//img"));
            const imgSrc = await img.getAttribute("src");
            const  imgDataSrc = await img.getAttribute("data-src");
            const src = imgSrc ? imgSrc : imgDataSrc;
            
            const a = await block.findElement(By.xpath("//a[@title]"));
            const href = await a.getAttribute("href");
            const title = await a.getAttribute("title");
            console.log( i, href, title )
            
            // if(  )
        }
    } catch (e) {
        console.log(e);
    } finally {
        await driver.quit();
    }
}
