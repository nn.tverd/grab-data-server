// const csv = require("csvtojson");
const axios = require("axios");
const fs = require("fs");
const Path = require("path");

const config = require("./configs/config.js");
// http://${config.serverIp}:${config.serverPort}


// const axios = require('axios');
// const e = require("express");
// const fs = require("fs");
// const csvFilePath = "./data-sources/export-parts.csv";
// csv()
//     .fromFile(csvFilePath)
//     .then((jsonObj) => {
//         console.log(jsonObj);

/* ============================================================
  Function: Download Image
============================================================ */

const download_image = (url, image_path) =>
    axios({
        url,
        responseType: "stream",
    }).then(
        (response) =>
            new Promise((resolve, reject) => {
                response.data
                    .pipe(fs.createWriteStream(image_path))
                    .on("finish", () => resolve())
                    .on("error", (e) => reject(e));
            })
    );

/* ============================================================
  Function: Download Image
============================================================ */

async function downloadImage(url, path, fileName) {
    // const url = "https://unsplash.com/photos/AaEQmoufHLk/download?force=true";
    // const path = Path.resolve(__, "data-sources", "imgs", fileName);
    const writer = fs.createWriteStream(path);

    const response = await axios({
        url,
        method: "GET",
        responseType: "stream",
    });

    response.data.pipe(writer);

    return new Promise((resolve, reject) => {
        writer.on("finish", resolve);
        writer.on("error", reject);
    });
}

(async function ___main() {
    try {
        const res1 = await axios({
            method: "get",
            url: `http://${config.serverIp}:${config.serverPort}/gimage`,
            headers: {
                "Content-Type": "application/json",
            },
        });
        // console.log(res1);
        const data = res1.data;
        for (let i in data) {
            const image = data[i];
            const id = image._id;
            const imagePath = `./data-sources/imgs/${id}.jpeg`;
            if (fs.existsSync(imagePath)) {
                console.log("if (fs.existsSync(imagePath)) {", id);
                continue;
            }
            const fileName = `${id}.jpeg`;
            if (image.src.indexOf("base64") < 0) {
                console.log("if (image.src.indexOf(base64) < 0) {", id, i);
                await downloadImage(image.src, imagePath, fileName);
                // const example_image_1 = await download_image(
                //     image.src,
                //     imagePath
                // );
                // console.log(example_image_1.status); // true
                continue;
            } else {
                // continue;
                // console.log(image.src);
                var base64Data = image.src.split(",")[1]; // split with `,`
                fs.writeFile(imagePath, base64Data, "base64", function (
                    err,
                    data
                ) {
                    if (err) {
                        console.log("err", err);
                    }
                    // console.log(data, "data");
                });
                // if(i>100) break;
                if (!(i % 300)) {
                    console.log(i);
                }
            }
        }
    } catch (err) {
        console.log(err);
    }
})();

// });
