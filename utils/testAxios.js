// // const HttpProxyAgent = require("http-proxy-agent");
// // const proxy = `http://49.0.82.190:8080`;

// // // curl -х 49.0.82.190:8080 -л ' https://www.walmart.com/ip/50676589 '

// // const axiosDefaultConfig = {
// //     baseURL: "https://jsonplaceholder.typicode.com/posts",
// //     // proxy: {
// //     //     host: `http://49.0.82.190`,
// //     //     port: `8080`,
// //     //     protocol: `http`,
// //     // },
// //     proxy: false,
// //     httpsAgent: new HttpProxyAgent(proxy),
// // };
// // const axios = require("axios").create(axiosDefaultConfig);
// // npm install axios axios-https-proxy-fix

// // const fetchUrl = `http://catalog.phaeton.kz/`;
// // const res = await axios(fetchUrl);
// // const text = await res.data();
// // console.log(text);

// const https = require('https');

// const axiosDefaultConfig = {
//     baseURL: "http://catalog.phaeton.kz/",
//     // proxy: {
//     //     host: `109.170.64.98`,
//     //     port: `3629`,
//     //     protocol: "http",
//     // },
// };

// // At request level
// const agent = new https.Agent({
//     rejectUnauthorized: false,
//     proxy: {
//         host: `109.170.64.98`,
//         port: `3629`,
//         protocol: "http",
//     },
// });

// // const axios = require("axios")
// // // .create(axiosDefaultConfig);
// // axios
// //     .get("http://catalog.phaeton.kz")
// //     .then(function (response) {
// //         console.log("Response with axios was ok: " + response.status);
// //     })
// //     .catch(function (error) {
// //         console.log(
// //             `==================\n`,
// //             `axios`,
// //             `\n==================\n\n`
// //         );
// //         console.log(error);
// //     });

// const axiosFixed = require("axios-https-proxy-fix").create(axiosDefaultConfig);
// axiosFixed
//     .get("totalcatalog", { httpsAgent: agent })
//     .then(function (response) {
//         console.log(
//             "Response with axios-https-proxy-fix was ok: " + response.status
//         );
//     })
//     .catch(function (error) {
//         console.log(
//             `==================\n`,
//             `axiosFixed`,
//             `\n==================\n\n`
//         );
//         console.log(error);
//     });

// 82.200.233.4	3128

// const axios = require("axios");
// const SocksProxyAgent = require("socks-proxy-agent");
// const HttpsProxyAgent = require("https-proxy-agent");
// const HttpProxyAgent = require("http-proxy-agent");

// // const agent = new HttpsProxyAgent({
// //     host: "95.111.245.169",
// //     port: "3128",
// // });
// const agent = new HttpProxyAgent({
//     host: "95.111.245.169",
//     port: "3128",
// });

// // replace with your proxy's hostname and port
// const proxyHost = `88.202.177.242`,
//     proxyPort = `1080`;
// // the full socks5 address
// const proxyOptions = `socks5://${proxyHost}:${proxyPort}`;
// // create the socksAgent for axios
// const httpsAgent = new SocksProxyAgent(proxyOptions);
// // the baseUrl for the api you are going to hit
// // const baseUrl = "https://google.com";
// const baseUrl = "http://catalog.phaeton.kz/";
// // create a new axios instance  http://82.200.233.4:3128   http://95.111.245.169:3128
// // const client = axios.create({ httpsAgent });
// // const client = axios.create({
// //     httpsAgent: agent,
// // });
// const client = axios;

// (async function __m() {
//     try {
//         const googlePage = await client.get(baseUrl, { httpsAgent: agent });
//         console.log(googlePage);
//     } catch (err) {
//         console.log(err);
//     }
// })();

// var url = require('url');
// var http = require('http');
// var HttpProxyAgent = require('http-proxy-agent');

// // HTTP/HTTPS proxy to connect to
// var proxy = 'http://95.111.245.169:3128';
// console.log('using proxy server %j', proxy);

// // HTTP endpoint for the proxy to connect to
// var endpoint = 'http://catalog.phaeton.kz/';
// console.log('attempting to GET %j', endpoint);
// var opts = url.parse(endpoint);

// // create an instance of the `HttpProxyAgent` class with the proxy server information
// var agent = new HttpProxyAgent(proxy);
// opts.agent = agent;

// http.get(opts, function (res) {
//   console.log('"response" event!', res.headers);
//   res.pipe(process.stdout);
// });

const { makeGetReq, makeGetReqAtempts } = require("./_makeGetReq.js");
const { getProxy } = require("./_getProxy.js");

(async function _m() {
    // const prox = getProxy();
    var endpoint = "http://catalog.phaeton.kz/";
    const result = await makeGetReqAtempts(endpoint);
    console.log(result.status);

    // for (let i = 0; i < 50; i++) {
    //     try {
    //         if (result.status == 200) {
    //             i = 1000;
    //         }
    //     } catch (err) {
    //         console.log(err);
    //     }
    // }
})();
// console.log(goodProxies);

// (async function _m() {
//     const prox = getProxies();
//     var endpoint = "http://catalog.phaeton.kz/";
//     const goodProxies = [];
//     let startTime = new Date();
//     for (let i in prox) {
//         try {
//             const result = await makeGetReq2(endpoint, prox[i]);

//             goodProxies.push(result);
//             const newDate = new Date();
//             console.log(result, goodProxies, newDate.getTime() - startTime.getTime());
//             startTime = newDate;
//         } catch (err) {
//             console.log(err);
//             console.log(prox[i], " to delete\n==================\n");
//         }
//     }
//     console.log(goodProxies);
// })();
