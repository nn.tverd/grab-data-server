// const csv = require("csvtojson");
const axios = require("axios");
const e = require("express");
const config = require("./configs/config.js");
// http://${config.serverIp}:${config.serverPort}

// const csvFilePath = "./data-sources/export-parts.csv";
// csv()
//     .fromFile(csvFilePath)
//     .then((jsonObj) => {
//         console.log(jsonObj);
(async function ___main() {
    try {
        const res1 = await axios({
            method: "get",
            url: `http://${config.serverIp}:${config.serverPort}/part`,
            headers: {
                "Content-Type": "application/json",
            },
        });
        console.log(res1);
        const data = res1.data;
        for (let i in data) {
            const part = data[i];
            const id = part._id;
            const partNumber = part.partNumber;
            const searchDet = {
                cond: { partNumberPretendToBe: partNumber },
                limit: 1
            }
            const res2 = await axios({
                method: "post",
                url: `http://${config.serverIp}:${config.serverPort}/gimage/search`,
                data: searchDet,
                headers: {
                    "Content-Type": "application/json",
                },
            });
            const resLength = res2.data.length;
            console.log( resLength )
            // const Part = {
            //     googleSearchImgWasMade: true
            // }
            if( !resLength ){
                await axios({
                    method: "patch",
                    url: `http://${config.serverIp}:${config.serverPort}/part/${id}`,
                    data: {
                        googleSearchImgWasMade: false
                    },
                    headers: {
                        "Content-Type": "application/json",
                    },
                })
            }
            else{
                await axios({
                    method: "patch",
                    url: `http://${config.serverIp}:${config.serverPort}/part/${id}`,
                    data: {
                        googleSearchImgWasMade: true
                    },
                    headers: {
                        "Content-Type": "application/json",
                    },
                })
            }
            
        }
    } catch (err) {
        console.log(err);
    }
})();

// });
