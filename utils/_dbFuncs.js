const axios = require("axios");
const config = require("./configs/config.js");

const getToTreatWithConditions = async (reqData) => {
    /*
    const reqData = {
        cond: {
            nextTaskName: "resetIdInInputDocs",
            requestType: "parts class acquisition",
            // requestType: "car modification acqusition additional information",
        },
        limit: 20000,
    };
    */
    const res = await axios({
        // 1
        method: "post",
        url: `http://${config.serverIp}:${config.serverPort}/inputdocs/gettotreat`,
        data: reqData,
        headers: {
            "Content-Type": "application/json",
        },
    });
    return res;
};

const getManyByIds = async (idsList) => {
    /*
    const idsList = ["id1", "id2"];
    */
    try {
        const res = await axios({
            // 1 gettotreat
            method: "post",
            url: `http://${config.serverIp}:${config.serverPort}/inputdocs/getManyByIdList`,
            data: idsList,
            headers: {
                "Content-Type": "application/json",
            },
        });
        return res;
    } catch (err) {
        console.log(err);
        return [];
    }
};

const updateManyByFullDoc = async (docsList) => {
    /*
    const docsList = [{it must include all the information of the document}, {}];
    */
    var ix,
        jx,
        temparray,
        chunk = 2000;
    for (ix = 0, jx = docsList.length; ix < jx; ix += chunk) {
        temparray = docsList.slice(ix, ix + chunk);
        try {
            const res = await axios({
                // 1 gettotreat
                method: "post",
                url: `http://${config.serverIp}:${config.serverPort}/inputdocs/updatemany`,
                data: temparray,
                headers: {
                    "Content-Type": "application/json",
                },
            });
            // return res;
            console.log(res.data);
        } catch (err) {
            console.log(err);
            return err;
        }
    }
    return { message: "results are updated" };
};

const updateInputDocAfterUse = (
    partsClass,
    filename,
    taskName,
    treatedByAlgoriths,
    nextTaks
) => {
    const algorithmRecord = {
        filename: filename,
        task: taskName,
        date: new Date(),
    };
    if (partsClass.treatedByAlgorithms) {
        partsClass.treatedByAlgorithms[filename] = algorithmRecord;
    } else {
        const record = {};

        record[filename] = algorithmRecord;
        partsClass.treatedByAlgorithms = { ...record };
    }
    partsClass.treatedByAlgoriths.push(treatedByAlgoriths),
        (partsClass.nextTaskName = nextTaks);
    partsClass.isUnderTreatment = 0;
};

module.exports.getToTreatWithConditions = getToTreatWithConditions;
module.exports.getManyByIds = getManyByIds;
module.exports.updateManyByFullDoc = updateManyByFullDoc;
module.exports.updateInputDocAfterUse = updateInputDocAfterUse;
