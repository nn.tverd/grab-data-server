// const csv = require("csvtojson");
const axios = require("axios");
const config = require("./configs/config.js");
// http://${config.serverIp}:${config.serverPort}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
const blockExec = false;
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

(async function __main() {
    console.log("set task to input docs starting .....");
    if (blockExec) {
        console.log("blockExec");
        return 0;
    }
    let iStep = 0;
    const startDate = new Date();
    // for (;;) {
    try {
        const reqData = {
            cond: {
                // "payload.makeName": "CHEVROLET",
                // // requestType: "car model acqusition",
                // requestType:
                //     "car modification acqusition additional information",
            },
            limit: 100000,
        };
        const res = await axios({
            // 1 gettotreat
            method: "post",
            url: `http://${config.serverIp}:${config.serverPort}/inputdocs/search`,
            data: reqData,
            headers: {
                "Content-Type": "application/json",
            },
        });
        console.log("startDate", startDate);
        console.log("current date", new Date());
        console.log(res.data.length);
        // return;
        const data = res.data;
        if (data.length === 0) {
            console.log("set task to input docs finishing .....");
            return;
        }
        for (let i in data) {
            const doc = data[i];
            if (!doc.payload.name.toLowerCase().includes("aveo")) {
                continue;
            }

            reqData.cond = {
                "payload.carModificationId": doc._id,
                // requestType: "car model acqusition",
                // requestType: "car modification acqusition additional information",
            };
            const res22 = await axios({
                // 1 gettotreat
                method: "post",
                url: `http://${config.serverIp}:${config.serverPort}/inputdocs/results`,
                data: reqData,
                headers: {
                    
                    "Content-Type": "application/json",
                },
            });

            console.log(
                doc.payload.name,
                doc.requestType,
                doc._id,
                res22.data.length
            );
            const items = res22.data;
            for (let k in items) {
                const item = items[k];
                item.payload["makeName"] = doc.payload.makeName;
                item.payload["modelName"] = doc.payload.modelName;
                delete item.date;

                const res32 = await axios({
                    method: "patch",
                    url: `http://${config.serverIp}:${config.serverPort}/inputdocs/${item._id}`,
                    data:item ,
                    headers: {
                        "Content-Type": "application/json",
                    },
                });
    
                console.log(++iStep, res32.data, item._id);
            }
            // return;
        }
    } catch (e) {
        console.log(e);
    }
    // }
})();

// });
