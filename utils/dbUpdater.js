// const csv = require("csvtojson");
const axios = require("axios");
const config = require("./configs/config.js");
// http://${config.serverIp}:${config.serverPort}
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
const blockExec = false;
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
async function updateSomething() {
    axios({
        method: "get",
        url: `http://${config.serverIp}:${config.serverPort}/inputdocs`,
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((res) => {
            console.log(res);
            const data = res.data;
            for (let i in data) {
                const part = data[i];
                const id = part._id;
                const payload = {
                    ...part.payload,
                    treatedByClassDownloader: false,
                };
                console.log(payload);
                break;
                axios({
                    method: "patch",
                    url: `http://${config.serverIp}:${config.serverPort}/part/${id}`,
                    data: Part,
                    headers: {
                        "Content-Type": "application/json",
                    },
                })
                    .then((res) => {
                        console.log(res);

                        // dispatch({ type: UPDATE_ABSTRACT_PART_TO_SHOW, payload: { abstractPartToShow: abstractPartToShow, abstractPartToShowIds: abstractPartToShowIds } });
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            }

            // dispatch({ type: UPDATE_ABSTRACT_PART_TO_SHOW, payload: { abstractPartToShow: abstractPartToShow, abstractPartToShowIds: abstractPartToShowIds } });
        })
        .catch((err) => {
            console.log(err);
        });

    // });
}
//-------------------------------------------------------------------------------
async function updateIsUnderTreatment() {
    try {
        const smpl = await axios({
            method: "get",
            url: `http://${config.serverIp}:${config.serverPort}/inputdocs/sample/2000`,
            headers: {
                "Content-Type": "application/json",
            },
        });
        console.log(smpl.data.length);

    } catch (e) {
        console.log(e);
    }
}
//-------------------------------------------------------------------------------

(async function __start() {
    console.log("dbUpdater starting.....");

    if (blockExec) {
        console.log(console.log("dbUpdater blocked....."));
        return;
    }
    // await updateSomething();
    updateIsUnderTreatment();
})();
