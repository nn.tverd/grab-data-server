const fs = require("fs");

const getLocalConfig = () => {
    const rawJson = fs.readFileSync("./utils/configs/localconfig.json");
    const json = JSON.parse(rawJson);
    return json;
};

module.exports.getLocalConfig = getLocalConfig;