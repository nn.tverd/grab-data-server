const puppeteer = require("puppeteer");
const axios = require("axios");
// const { promisify } = require("util");
const config = require("./configs/config.js");
// http://${config.serverIp}:${config.serverPort}



const link_base = "https://www.google.ru/search?tbm=isch&q=";
// const link_base = "https://www.google.ru/search?tbm=isch&q=GM+96169594";

//-------------------------------------------------------------------------------
async function getParts() {
    try {
        const response = await axios({
            method: "get",
            url: `http://${config.serverIp}:${config.serverPort}/part`,
            headers: {
                "Content-Type": "application/json",
            },
        });
        return response.data;
    } catch (err) {
        console.log(err);
        return [];
    }
}
//-------------------------------------------------------------------------------
// const sleep = require("util").promisify(setTimeout)(
function timeout(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
//-------------------------------------------------------------------------------
(async () => {
    // const link = `${link_base}`;
    const parts = await getParts();
    const browser = await puppeteer.launch({ headless: false });

    const page = await browser.newPage();

    for (let i in parts) {
        const part = parts[i];
        if (part.googleSearchImgWasMade) continue;
        console.log("newPartSearch 1 ");
        await timeout(7000 + Math.random() * 6000);
        console.log("newPartSearch 2 ");
        const link = `${link_base}${part.brand}+${part.partNumber}`;

        await page.goto(link, { waitUntil: "networkidle2" });

        let images = await page.evaluate(() => {
            const frame = document.getElementById("islrg");
            if (!frame) return {};
            const blocks = frame.querySelectorAll("div[jsaction]");
            const images = [];
            for (let ib in blocks) {
                const block = blocks[ib];
                if (!block.querySelector) continue;
                const img = block.querySelector("img");
                if (!img) continue;
                let src = img.getAttribute("src");
                if (!src) src = img.getAttribute("data-src");
                // const imgSrc = img.getAttribute("data-src");
                const imgAlt = img.getAttribute("alt");
                // if (!imgSrc) {
                //     debugger;
                // }
                // const imgDataSrc = img.getAttribute('src');
                const a = block.querySelector("a[title]");
                if (!a) continue;
                const href = a.getAttribute("href");
                const title = a.getAttribute("title");
                images.push({
                    src: src,
                    alt: imgAlt,
                    href,
                    title,
                    // partNumberPretendToBe: part.partNumber,
                    // brandPretendToBe: part.brand,
                    // analogsGroup: part.analogsGroup,
                });
            }

            return images;
        });
        for (let idx in images) {
            images[idx].partNumberPretendToBe = part.partNumber;
            images[idx].brandPretendToBe = part.brand;
            images[idx].analogsGroup = part.analogsGroup;
            const response = await axios({
                method: "post",
                url: `http://${config.serverIp}:${config.serverPort}/gimage`,
                data: images[idx],
                headers: {
                    "Content-Type": "application/json",
                },
            });
            console.log(i, response.status);
        }
        // console.log(images[0]);
        // console.log(images[1]);

        // if (i > 1) break;
        const response = await axios({
            method: "patch",
            url: `http://${config.serverIp}:${config.serverPort}/part/${part._id}`,
            data: {
                googleSearchImgWasMade: true,
            },
            headers: {
                "Content-Type": "application/json",
            },
        });
        console.log(i, response.status);
    }

    // debugger;
    await browser.close();
})();
