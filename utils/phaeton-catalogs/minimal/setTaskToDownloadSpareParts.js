// const csv = require("csvtojson");
const {
    getToTreatWithConditions,
    getManyByIds,
    updateManyByFullDoc,
} = require("../../_dbFuncs.js");
// http://${config.serverIp}:${config.serverPort}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
const blockExec = false;
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

(async function __main() {
    console.log("setTaskToDownloadSpareParts to input docs starting .....");
    if (blockExec) {
        console.log("blockExec");
        return 0;
    }
    let iStep = 0;
    const startDate = new Date();
    for (;;) {
        try {
            const reqData = {
                cond: {
                    // "payload.makeName": "CHEVROLET",
                    // requestType: "car model acqusition",
                    requestType: "parts class acquisition",
                },
                limit: 40000,
            };
            const res = await getToTreatWithConditions(reqData);
            console.log("startDate", startDate);
            console.log("current date", new Date());
            console.log(res.data.length);

            // return;
            const data = res.data;
            if (data.length === 0) {
                console.log("set test to input docs finishing .....");
                return;
            }
            const modelIds = [];
            for (let i in data) {
                const doc = data[i];
                doc.nextTaskName = "13";
                modelIds.push(doc.payload.modelId);
            }
            const modelsRes = await getManyByIds(modelIds);
            const models = modelsRes.data;
            for (let i in models) {
                const model = models[i];
                model.nextTaskName = "12";
            }
            const result = models.concat(data);
            console.log("iStep", iStep++);
            console.log("data.length", data.length);
            console.log("models.length", models.length);
            console.log("result.length", result.length);

            const updRes = await updateManyByFullDoc(result);
            console.log("updRes", updRes);
        } catch (e) {
            // console.log("e");
            console.log(e);
        }
    }
})();

// });
