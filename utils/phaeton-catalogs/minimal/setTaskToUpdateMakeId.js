/**
 * here we are updating "parts class acquisition"
 * in order to get
 * carModificationName
 * modelName
 * modelId
 * makeName
 * makeId
 *
 */
const taskName = `here we are updating "parts class acquisition" in order to get * carModificationName * modelName * modelId * makeName * makeId`;
const filename = `utils/phaeton-catalogs/minimal/setTaskToUpdateMakeId.js`;
const {
    getToTreatWithConditions,
    getManyByIds,
    updateManyByFullDoc,
} = require("../../_dbFuncs.js");
// http://${config.serverIp}:${config.serverPort}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
const blockExec = false;
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

(async function __main() {
    console.log("setTaskToUpdateMakeId to input docs starting .....");
    if (blockExec) {
        console.log("blockExec");
        return 0;
    }
    let iStep = 0;
    const startDate = new Date();
    for (;;) {
        try {
            const reqData = {
                cond: {
                    // "payload.makeName": "CHEVROLET",
                    // requestType: "car model acqusition",
                    nextTaskName: { $ne: "downloadParts" },
                    requestType: "parts class acquisition",
                },
                limit: 40000,
            };
            const _classes = await getToTreatWithConditions(reqData);

            console.log("startDate", startDate);
            console.log("current date", new Date());
            console.log(_classes.data.length);

            // return;
            const classes = _classes.data;
            console.log("classes.length", classes.length);

            if (classes.length === 0) {
                console.log("set test to input docs finishing .....");
                return;
            }
            const modificationsIds = [];
            const classes__ = {};
            for (let i in classes) {
                const oneClass = classes[i];
                classes__[oneClass._id] = oneClass;
                oneClass.nextTaskName = "downloadParts";
                oneClass.isUnderTreatment = false;
                modificationsIds.push(oneClass.payload.carModificationId);
                const algorithmRecord = {
                    filename: filename,
                    task: taskName,
                    date: new Date(),
                };
                if (oneClass.treatedByAlgorithms) {
                    oneClass.treatedByAlgorithms[filename] = algorithmRecord;
                } else {
                    const record = {};

                    record[filename] = algorithmRecord;
                    oneClass.treatedByAlgorithms = { ...record };
                }
            }

            const _modifications = await getManyByIds(modificationsIds);
            const modifications = _modifications.data;
            console.log("modifications.length", modifications.length);
            const modelIds = [];
            const modification__ = {};
            for (let i in modifications) {
                const modification = modifications[i];
                modelIds.push(modification.payload.modelId);
                modification__[modification._id] = modification;
                modification.nextTaskName = "";
                const algorithmRecord = {
                    filename: filename,
                    task: taskName,
                    date: new Date(),
                };
                if (modification.treatedByAlgorithms) {
                    modification.treatedByAlgorithms[
                        filename
                    ] = algorithmRecord;
                } else {
                    const record = {};

                    record[filename] = algorithmRecord;
                    modification.treatedByAlgorithms = { ...record };
                }
            }

            const _models = await getManyByIds(modelIds);
            const models = _models.data;
            console.log("models.length", models.length);
            const makeIds = [];
            const model__ = {};
            for (let i in models) {
                const model = models[i];
                makeIds.push(model.payload.makeId);
                model__[model._id] = model;
                model.nextTaskName = "";

                const algorithmRecord = {
                    filename: filename,
                    task: taskName,
                    date: new Date(),
                };
                if (model.treatedByAlgorithms) {
                    model.treatedByAlgorithms[filename] = algorithmRecord;
                } else {
                    const record = {};

                    record[filename] = algorithmRecord;
                    model.treatedByAlgorithms = { ...record };
                }
            }

            const _makes = await getManyByIds(makeIds);
            const makes = _makes.data;
            console.log("makes.length", makes.length);
            const make__ = {};
            for (let i in makes) {
                const make = makes[i];
                make__[make._id] = make;
                make.nextTaskName = "";
                const algorithmRecord = {
                    filename: filename,
                    task: taskName,
                    date: new Date(),
                };
                if (make.treatedByAlgorithms) {
                    make.treatedByAlgorithms[filename] = algorithmRecord;
                } else {
                    const record = {};

                    record[filename] = algorithmRecord;
                    make.treatedByAlgorithms = { ...record };
                }
                // make.treatedByAlgorithms[filename] = {
                //     filename: filename,
                //     task: taskName,
                //     date: new Date(),
                // };
            }

            for (let i in classes) {
                const oneClass = classes[i];
                const modif =
                    modification__[oneClass.payload.carModificationId];
                // console.log(modif.payload.modelId);
                const model = model__[modif.payload.modelId];
                // console.log(model, model.payload.makeId);
                const make = make__[model.payload.makeId];
                oneClass.payload.carModificationName = modif.payload.name;
                oneClass.payload.modelName = model.payload.name;
                oneClass.payload.makeName = make.payload.name;

                oneClass.payload.modelId = model._id;
                oneClass.payload.makeId = make._id;

                // console.log(oneClass);
                // return;
            }
            // console.log(classes[0]);

            const result = classes
                .concat(modifications)
                .concat(models)
                .concat(makes);
            console.log("iStep", iStep++);
            console.log("result.length", result.length);
            console.log("classes.length", classes.length);
            console.log("modifications.length", modifications.length);
            console.log("models.length", models.length);
            console.log("makes.length", makes.length);
            // return;

            const updRes = await updateManyByFullDoc(result);
            console.log("updRes", updRes);
        } catch (e) {
            // console.log("e");
            console.log(e);
        }
    }
})();

// });
