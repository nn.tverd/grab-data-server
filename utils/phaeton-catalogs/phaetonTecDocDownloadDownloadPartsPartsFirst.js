// if there is no search button to click we shoul skip this item
// 96243577

/**
 * here we run script to recieve parts from catalogs
 * in order to get
 * carModificationName
 * modelName
 * modelId
 * makeName
 * makeId
 *
 */
const taskName = `downloadParts`;
const filename = `utils/phaeton-catalogs/phaetonTecDocDownloadDownloadPartsPartsFirst`;
const {
    getToTreatWithConditions,
    getManyByIds,
    updateInputDocAfterUse,
    updateManyByFullDoc,
} = require("../_dbFuncs.js");
const { getLocalConfig } = require("../configs/getLocalConfig.js");

const { makeGetReq, makeGetReqAtempts } = require("../_makeGetReq.js");
// var HttpProxyAgent = require("http-proxy-agent");

// const fetch = require('node-fetch-with-proxy');

const puppeteer = require("puppeteer");
const fetch = require("node-fetch");
const axios = require("axios");
const cheerio = require("cheerio");

const config = require("../configs/config.js");
// http://${config.serverIp}:${config.serverPort}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
const blockExec = false;
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
const params = {
    startUrl: "http://catalog.phaeton.kz",
    requestTypeToSearch: "parts class acquisition",
    requestType: "parts initial downloads",
    makeName: "CHEVROLET",
    treatedByAlgoriths:
        "phaeton-catalogs/phaetonTecDocDownloadDownloadPartsPartsFirst.js",
    nextTaskName: "downloadParts",
    nextTaskNameToSet: "consolidateParts",
};
//-------------------------------------------------------------------------------
async function getPartsClasses() {
    try {
        const localConfig = getLocalConfig();
        const response = await axios({
            method: "post",
            url: `http://${config.serverIp}:${config.serverPort}/inputdocs/gettotreat`,
            data: {
                cond: {
                    "payload.makeName": params.makeName,
                    requestType: params.requestTypeToSearch,
                    nextTaskName: params.nextTaskName,
                },
                limit: localConfig.samleSize1,
                // limit: 1,
            },
            headers: {
                "Content-Type": "application/json",
            },
        });
        console.log(response.data.length);
        return response.data;
        // return new Promise((resolve) => {
        //     resolve(response.data);
        // });
    } catch (err) {
        console.log(err);
        return [];
    }
}
//-------------------------------------------------------------------------------
// const sleep = require("util").promisify(setTimeout)(
function timeout(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
//-------------------------------------------------------------------------------
function extractDataFromClass(partsClass) {
    const maxLevel = partsClass.payload.maxlevel;
    const keys = Object.keys(partsClass.payload);
    let fetchUrl = "";
    const meta = {};
    for (let kk in keys) {
        const key = keys[kk];
        if (key.includes(`-level-${maxLevel}`)) {
            fetchUrl = partsClass.payload[key].link;
        }
        if (typeof partsClass.payload[key] == "object") {
            // console.log(
            //     "we have found object",
            //     key,
            //     partsClass.payload[key]
            // );
            // console.log(partsClass.payload.modelName);
            meta[`level-${maxLevel - partsClass.payload[key].level}`] =
                partsClass.payload[key].text;
            meta.makeName = partsClass.payload.makeName;
            meta.modelName = partsClass.payload.modelName;
            meta.carModificationId = partsClass.payload.carModificationId;
        }
    }
    return {
        meta: meta,
        fetchUrl: fetchUrl,
        maxLevel: maxLevel,
    };
}
//-------------------------------------------------------------------------------
function getListOfParts($) {
    const table = $('table[class="table2wt2"]');

    // return;
    const list = $(table)
        .find("tr")
        .toArray()
        .map((element) => {
            return $(element)
                .find("td")
                .toArray()
                .map((tdEl, index) => {
                    return $(tdEl).html();
                });
        });
    return list;
}
//-------------------------------------------------------------------------------
function parseDataFromList(list, meta) {
    let lastBrand = "";
    let offset = 0;
    for (let l in list) {
        const row = list[l];
        if (row.length == 0) continue;
        if (row.length >= 7) {
            lastBrand = row[0];
            offset = 0;
        } else {
            offset = 1;
        }
        let brandName = lastBrand;

        const partNumber = row[1 - offset];
        const name = cheerio.load(row[2 - offset]).text();
        const shortInfo = cheerio
            .load(row[3 - offset])
            .text()
            .trim();
        // ----
        const $infoLink = cheerio.load(row[4 - offset]);
        let infoLink = "";
        if ($infoLink) {
            const oc = $infoLink(`input`).attr(`onclick`);
            infoLink = oc.split(`/catalog.phaeton.kz/`)[1].split(`');`)[0];
            // console.log(infoLink);
        }
        // ----
        const $carsLink = cheerio.load(row[5 - offset]);
        let carsLink = "";
        if ($carsLink) {
            const oc = $carsLink(`input`).attr(`onclick`);

            carsLink = oc.split(`/catalog.phaeton.kz/`)[1].split(`');`)[0];
            // console.log(carsLink);
        }
        // ----
        const $imagesLink = cheerio.load(row[6 - offset]);
        let imagesLink = "";
        if ($imagesLink) {
            const oc = $imagesLink(`input`).attr(`onclick`);
            imagesLink = oc.split(`/catalog.phaeton.kz/`)[1].split(`');`)[0];
            // console.log(imagesLink);
        }

        // console.log(brandName, partNumber, name, shortInfo);
        const obj = {
            ...meta,

            brandName: brandName,
            partNumber: partNumber,
            name: name,
            shortInfo: shortInfo,
            infoLink: infoLink,
            carsLink: carsLink,
            imagesLink: imagesLink,
        };
        list[l] = obj;
    }
}
//-------------------------------------------------------------------------------
function createItemToWrite(payload, fetchUrl, partsClass) {
    const algorithmRecord = {
        [filename]: {
            filename: filename,
            task: taskName,
            date: new Date(),
        },
    };

    const item = {
        sourse: params.startUrl,
        requestId: fetchUrl,
        requestName: partsClass.payload.carModificationId,
        requestType: params.requestType,
        treatedByAlgoriths: [params.treatedByAlgoriths],
        nextTaskName: params.nextTaskNameToSet,
        treatedByAlgorithms: { ...algorithmRecord },
        payload: { ...payload },
    };
    return item;
}
//-------------------------------------------------------------------------------
function isFinish(partsClasses) {
    const partsClassesCount = partsClasses.length;
    if (partsClassesCount < 1) {
        console.log("fetching finish........");
        return true;
    }
    console.log("isFinish........", partsClassesCount);

    return false;
}
//-------------------------------------------------------------------------------
function printStep(stepIndex, startDate, interval) {
    if (interval < 1) interval = 1;
    if (stepIndex % interval == 0) {
        console.log(stepIndex, startDate, new Date());
    }
}
//-------------------------------------------------------------------------------
function isDocsAlreadyTreated(partsClass) {
    const { treatedByAlgoriths, treatedByAlgorithms } = partsClass;
    console.log(partsClass.payload.modelName);
    // if (
    //     partsClass.payload.modelName &&
    //     !partsClass.payload.modelName.toLowerCase().includes("epica")
    // ) {
    //     continue;
    // }
    if (treatedByAlgorithms[filename]) return true;

    let alreadyTreated = false;
    // console.log(partsClass);
    for (let it in treatedByAlgoriths) {
        if (treatedByAlgoriths[it] == params.treatedByAlgoriths)
            alreadyTreated = true;
    }

    return alreadyTreated;
}
//-------------------------------------------------------------------------------
async function addOneItemToInputDocs(item) {
    const response2 = await axios({
        method: "post",
        url: `http://${config.serverIp}:${config.serverPort}/inputdocs`,
        data: item,
        headers: {
            "Content-Type": "application/json",
        },
    });
    console.log("writingToDB", response2.status);
}
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

const start_url = "http://catalog.phaeton.kz";

async function onPuppeteer() {
    const partsClasses = await getPartsClasses();
    console.log(partsClasses.length);

    const browser = await puppeteer.launch({
        headless: false,
        devtools: false,
    });
    console.log("reached step - 1");
    const page = await browser.newPage();
    console.log("reached step - 2");

    await page.setViewport({
        width: 1920,
        height: 1080,
    });
    console.log("reached step - 3");

    const partsClassesCount = partsClasses.length;
    const revPartsClassesCount = 1 / partsClassesCount;
    const startDate = new Date();
    let i = 0;
    console.log("reached step - 4");

    for (; i < partsClasses.length; i++) {
        if (i < 43300) continue;
        if (i % 100 == 0) {
            console.log(startDate);
            console.log(new Date());
            console.log(i, partsClassesCount, i * revPartsClassesCount);
        }
        try {
            const partsClass = partsClasses[i];
            const requestData = {
                sourse: params.startUrl,
                requestId: partsClass.payload.name,
                requestType: params.requestType,
            };

            await page.goto(start_url + partsClass.payload.link, {
                waitUntil: "networkidle2",
            });
            // click on describe all
            // await page.goto1(123);
            await page.evaluate(() => {
                const as = document.querySelectorAll(`a`);
                for (let iA in as) {
                    const a = as[iA];
                    if (!a.getAttribute) continue;
                    const onclick = a.getAttribute("onclick");
                    console.log("onclick", onclick);
                    if (onclick.includes("describe_all")) {
                        a.click();
                        return;
                    }
                }
            });
            // await page.waitForTimeout(100);
            // return;
            const addData = await page.evaluate(() => {
                const data = [];
                const div_tree = document.querySelector("#tree_div");
                const nodes = div_tree.querySelectorAll(`div.dtreeNode`);
                console.log(nodes.length);
                let oneObject = {
                    maxlevel: 0,
                };

                for (let iN in nodes) {
                    // if (i > 30) break;
                    const node = nodes[iN];
                    if (!node.querySelector) continue;
                    const table = node.querySelector(`table`);
                    // console.log("table.innerText", table.innerText);
                    const tds = table.querySelectorAll(`td`);
                    const level = tds.length;
                    // if (level < oneObject.maxlevel) {
                    //     console.log("push one object", i, level, oneObject);

                    //     data.push(JSON.parse(JSON.stringify(oneObject)));
                    //     oneObject = {
                    //         maxlevel: 0,
                    //     };
                    // }
                    let keys = Object.keys(oneObject);
                    console.log(oneObject, keys);
                    for (let k in keys) {
                        const key = keys[k];
                        if (key == "maxlevel") continue;
                        // console.log(k, key, oneObject[key]);
                        const innerEnt = oneObject[key];
                        if (!innerEnt) continue;
                        // console.log(k, key, innerEnt, Object.keys(innerEnt));
                        console.log(innerEnt.level, level, oneObject);
                        if (!innerEnt.level) continue;
                        if (innerEnt.level >= level) {
                            delete oneObject[key];
                            console.log("deletion", key, oneObject);
                            continue;
                        }
                    }

                    const td = tds[level - 1];
                    const aTd = td.querySelector("a");
                    const link = aTd.href;
                    const text = aTd.innerText;
                    const name = aTd.getAttribute(`name`);
                    oneObject[`node-${iN}-level-${level}`] = {
                        level: level,
                        name: name,
                        link: link,
                        text: text,
                    };
                    // console.log("update one object", i, level, oneObject);
                    oneObject.maxlevel = level;
                    if (link.includes(`st=50`)) {
                        oneObject = JSON.parse(JSON.stringify(oneObject));
                        // console.log("push one object", i, level, oneObject);

                        data.push(JSON.parse(JSON.stringify(oneObject)));
                    }
                    // console.log(table.innerText, tds.length);
                }
                console.log(data);
                return data;
            });

            console.log(i, addData.length);
            // return;
            // return
            // await page.waitForTimeout(1000);
            // await page.click(`toclickid${i}`);
            // await page.waitForTimeout(1000);

            for (let d in addData) {
                /*
                    const params = {
                        startUrl: "http://catalog.phaeton.kz",
                        requestType: "parts class acquisition",
                        requestTypeToSearch: "car modification acqusition additional information",
                        treatedByAlgoriths:
                            "phaeton-catalogs/phaetonTecDocDownloadDownloadPartsClasses.js",
                    };
                */
                const newPartsClasses = {
                    ...requestData,
                    treatedByAlgoriths: [params.treatedByAlgoriths],
                    payload: {
                        ...addData[d],
                        partsClassId: partsClass._id,
                    },
                };
                // console.log(newPartsClasses);
                // continue;
                try {
                    const response = await axios({
                        method: "post",
                        url: `http://${config.serverIp}:${config.serverPort}/inputdocs`,
                        data: newPartsClasses,
                        headers: {
                            "Content-Type": "application/json",
                        },
                    });
                    // console.log(response.data);
                } catch (err) {
                    console.log(err);
                }
            }
            // break;
        } catch (err) {
            console.log(i, "err", err);
            console.log(i);
            i -= 1;
            console.log(i);
            continue;
        }
    }
}
async function onFetch() {
    const startDate = new Date();
    let stepIndex = 0;
    for (;;) {
        console.log("new cycle started.....");
        const partsClasses = await getPartsClasses();
        // console.log( partsClasses );
        if (isFinish(partsClasses)) return 0;
        printStep(++stepIndex, startDate, 50);

        // console.log(partsClasses);

        for (let i in partsClasses) {
            const partsClass = partsClasses[i];
            if (isDocsAlreadyTreated(partsClass)) continue;
            // get URL to fetch
            const { fetchUrl, meta, maxLevel } = extractDataFromClass(
                partsClass
            );
            console.log(fetchUrl);
            // continue;
            try {
                const response = await makeGetReqAtempts(fetchUrl);
                const text = response.body;

                const $ = cheerio.load(text);
                const list = getListOfParts($);
                parseDataFromList(list, meta);

                // return;
                for (let l in list) {
                    // console.log(list);
                    const payload = list[l];
                    // console.log("payload", payload, list, list[l]);
                    // return;
                    payload.html = text;
                    payload.link = fetchUrl;

                    const item = createItemToWrite(
                        payload,
                        fetchUrl,
                        partsClass
                    );
                    await addOneItemToInputDocs(item);
                }
            } catch (err) {
                console.log(err);
            }

            // break;
            updateInputDocAfterUse(
                partsClass,
                filename,
                taskName,
                params.treatedByAlgoriths,
                ""
            );
        }
        // update part classes task and treated algorithms in data base
        const updRes = await updateManyByFullDoc(partsClasses);
        console.log("updRes", updRes);
    }
}

(async () => {
    if (blockExec) {
        console.log("blockExec");
        return 0;
    }
    console.log(`${params.treatedByAlgoriths}.....`);
    await onFetch();
    // await onPuppeteer();
})();
