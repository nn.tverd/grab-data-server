// if there is no search button to click we shoul skip this item
// 96243577
const puppeteer = require("puppeteer");
const fs = require("fs");
const axios = require("axios");

const config = require("../configs/config.js");
// http://${config.serverIp}:${config.serverPort}
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
const blockExec = true;
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
async function getParts() {
    try {
        const response = await axios({
            method: "get",
            url: `http://${config.serverIp}:${config.serverPort}/part`,
            headers: {
                "Content-Type": "application/json",
            },
        });
        return response.data;
    } catch (err) {
        console.log(err);
        return [];
    }
}
//-------------------------------------------------------------------------------
// const sleep = require("util").promisify(setTimeout)(
function timeout(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
//-------------------------------------------------------------------------------
async function autoScroll(page) {
    await page.evaluate(async () => {
        await new Promise((resolve, reject) => {
            var totalHeight = 0;
            var distance = 100;
            var timer = setInterval(() => {
                var scrollHeight = document.body.scrollHeight;
                window.scrollBy(0, distance);
                totalHeight += distance;

                if (totalHeight >= scrollHeight) {
                    clearInterval(timer);
                    resolve();
                }
            }, 100);
        });
    });
}
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

const start_url = "http://catalog.phaeton.kz/totalcatalog";

(async () => {
    console.log("phaetonTecDocDownloadDownloadCarBrands.....");
    if (blockExec) {
        console.log("blockExec");
        return 0;
    }
    const startDate = new Date();
    const browser = await puppeteer.launch({
        headless: false,
        devtools: false,
    });

    const page = await browser.newPage();

    await page.setViewport({
        width: 1920,
        height: 10800,
    });
    const requestData = {
        sourse: start_url,
        requestId: "init",
        requestType: "car make acqusition",
        algorithmName:
            "phaeton-catalogs/phaetonTecDocDownloadDownloadCarBrands.js",
        treatedByAlgoriths: [
            "phaeton-catalogs/phaetonTecDocDownloadDownloadCarBrands.js",
        ],
    };

    await page.goto(start_url, { waitUntil: "networkidle2" });
    await page.waitForTimeout(3000);
    const makes = await page.evaluate(() => {
        const table = document.querySelector(`table[class="content_table"]`);
        const as = table.querySelectorAll(`a`);
        console.log(as.length);
        const data = [];
        for (let i in as) {
            const payload = {};
            const a = as[i];
            if (!a.getAttribute) continue;
            payload.link = a.getAttribute("href");
            payload.name = a.innerText;
            data.push(payload);
        }
        return data;
    });
    console.log(makes.length);
    for (let i in makes) {
        const item = {
            ...requestData,
            payload: {
                ...makes[i],
            },
        };
        try {
            const response = await axios({
                method: "post",
                url: `http://${config.serverIp}:${config.serverPort}/inputdocs`,
                data: item,
                headers: {
                    "Content-Type": "application/json",
                },
            });
            console.log(response);
        } catch (err) {
            console.log(err);
        }
        // break;
    }
})();
