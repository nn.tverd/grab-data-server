// if there is no search button to click we shoul skip this item
// 96243577
const puppeteer = require("puppeteer");
const axios = require("axios");

const { getProxy } = require("../_getProxy.js");
const config = require("../configs/config.js");
// http://${config.serverIp}:${config.serverPort}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
const blockExec = false;
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
async function getCarModels() {
    try {
        const response = await axios({
            method: "post",
            url: `http://${config.serverIp}:${config.serverPort}/inputdocs/search`,
            data: {
                cond: { requestType: "car model acqusition" },
                limit: 100000,
            },
            headers: {
                "Content-Type": "application/json",
            },
        });
        console.log(response.data.length);
        return response.data;
        // return new Promise((resolve) => {
        //     resolve(response.data);
        // });
    } catch (err) {
        console.log(err);
        return [];
    }
}
//-------------------------------------------------------------------------------
// const sleep = require("util").promisify(setTimeout)(
function timeout(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

const start_url = "http://catalog.phaeton.kz";

(async () => {
    if (blockExec) {
        console.log("blockExec");
        return 0;
    }
    console.log("phaetonTecDocDownloadDownloadCarModification.....");
    const startDate = new Date();

    const carModels = await getCarModels();
    console.log(carModels.length);

    const browser = await puppeteer.launch({
        headless: false,
        devtools: false,
        args: [`--proxy-server=${getProxy()}`],
    });
    const page = await browser.newPage();

    await page.setViewport({
        width: 1920,
        height: 1080,
    });

    for (let i in carModels) {
        const carModel = carModels[i];
        const requestData = {
            sourse: start_url,
            requestId: carModel.payload.name,
            requestType: "car modification acqusition",
            algorithmName:
                "phaeton-catalogs/phaetonTecDocDownloadDownloadCarModification.js",
            treatedByAlgoriths: [
                "phaeton-catalogs/phaetonTecDocDownloadDownloadCarModification.js",
            ],
        };

        await page.goto(start_url + carModel.payload.link, {
            waitUntil: "networkidle2",
        });
        await page.waitForTimeout(500);
        // return;
        const models = await page.evaluate(() => {
            // const div_loadpage = document.querySelector(`#loadpage`);
            // const table_class = div_loadpage.querySelector(
            //     `table[class="table2wt content_table"]`
            // );
            // const table = table_class.querySelector(`table`);
            const trs = document.querySelectorAll(`tr.over`);
            console.log(trs.length);
            // return [];
            const data = [];
            for (let i in trs) {
                if (i === 0) continue;
                const payload = {};
                const tr = trs[i];
                console.log(tr.innerText);
                if (!tr.querySelector) continue;
                console.log(tr.innerText);

                const tds = tr.querySelectorAll(`td`);
                console.log("in browser", "step 1");
                for (let j in tds) {
                    console.log("in browser", "step 2", j, i);

                    const td = tds[j];
                    console.log("in browser", "step 2", j, i, td);

                    if (j == 0) {
                        console.log("in browser", "step 3", j, i);
                        const a = td.querySelector(`a`);
                        payload.name = a.innerText;
                        payload.link = a.getAttribute("href");
                    }
                    if (j == 1) {
                        console.log("in browser", "step 4", j, i);
                        payload.years = td.innerText;
                    }
                    if (j == 2) {
                        console.log("in browser", "step 5", j, i);
                        payload.power_kW = td.innerText;
                    }
                    if (j == 3) {
                        console.log("in browser", "step 6", j, i);
                        payload.power_horseforces = td.innerText;
                    }
                    if (j == 4) {
                        console.log("in browser", "step 7", j, i);
                        payload.engineCode = td.innerText;
                    }
                    if (j == 5) {
                        console.log("in browser", "step 8", j, i);
                        payload.engineVolume = td.innerText;
                    }
                    if (j == 6) {
                        console.log("in browser", "step 9", j, i);
                        payload.typeOfAssemble = td.innerText;
                    }
                    if (j == 7) {
                        console.log("in browser", "step 10", j, i);
                        payload.axleConfig = td.innerText;
                    }
                    if (j == 8) {
                        console.log("in browser", "step 11", j, i);
                        payload.availableWeight = td.innerText;
                    }
                    if (j == 9) {
                        console.log("in browser", "step 12", j, i);
                        const input = td.querySelector(`input[type='image']`);
                        const onclick = input.getAttribute("onclick");
                        const addUrl = onclick
                            .split(`catalog.phaeton.kz`)[1]
                            .split(`');`)[0];
                        payload.additionalInfoClick = addUrl;
                        // input.setAttribute(`id`, `toclickid${i}`);
                    }
                }

                console.log("payload", payload);
                data.push(payload);
                // break;
            }
            console.log(data);
            return data;
        });
        console.log(models);

        for (let i in models) {
            // await page.waitForTimeout(1000);
            // await page.click(`toclickid${i}`);
            // await page.waitForTimeout(1000);
            models[i].makeName = carModel.requestId;
            models[i].modelName = carModel.payload.name;
            models[i].modelId = carModel._id;
            const item = {
                ...requestData,
                payload: {
                    ...models[i],
                },
            };
            console.log(item);
            // continue;
            try {
                const response = await axios({
                    method: "post",
                    url: `http://${config.serverIp}:${config.serverPort}/inputdocs`,
                    data: item,
                    headers: {
                        "Content-Type": "application/json",
                    },
                });
                console.log(response.data);
            } catch (err) {
                console.log(err);
            }
            // break;
        }
        // break;
    }
})();
