// const csv = require("csvtojson");
const axios = require("axios");
const inputDocs = require("../../models/inputDocs.js");
const config = require("../configs/config.js");
// http://${config.serverIp}:${config.serverPort}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
const blockExec = false;
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

(async function __main() {
    console.log("checkIfChildExist starting .....");
    if (blockExec) {
        console.log("blockExec");
        return 0;
    }
    let i = 0;
    for (;;) {
        try {
            const reqData = {
                cond: {
                    nextTaskName: "checkIfChildExist",
                    requestType: "car model acqusition",
                    // requestType: "car modification acqusition additional information",
                },
                limit: 1000,
            };
            const res = await axios({
                // 1
                method: "post",
                url: `http://${config.serverIp}:${config.serverPort}/inputdocs/gettotreat`,
                data: reqData,
                headers: {
                    "Content-Type": "application/json",
                },
            });

            const data = res.data;
            if (data.length === 0) {
                console.log("checkIfChildExist finishing .....");
                return;
            }
            for (let i in data) {
                const doc = data[i];
                const reqId = doc.payload.link;
                console.log(reqId);
                const children = await axios({
                    method: "post",
                    url: `http://${config.serverIp}:${config.serverPort}/inputdocs/searchone`,
                    data: {
                        cond: {
                            requestType:
                                "car modification acqusition additional information",
                            requestId: reqId,
                        },
                        limit: 100,
                    },
                    headers: {
                        "Content-Type": "application/json",
                    },
                });
                const hasChild = children.data;
                console.log("hasChild", hasChild, doc.requestName);
                let newTaks = '';
                if (!hasChild) newTaks = "downloadChildren"
                const res2 = await axios({
                    method: "patch",
                    url: `http://${config.serverIp}:${config.serverPort}/inputdocs/${doc._id}`,
                    data: { nextTaskName: newTaks },
                    headers: {
                        "Content-Type": "application/json",
                    },
                });

                console.log(++i, res2.data);
            }
        } catch (e) {
            console.log(e);
        }
    }
})();

// });
