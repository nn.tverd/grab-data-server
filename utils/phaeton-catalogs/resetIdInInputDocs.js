// const csv = require("csvtojson");
const axios = require("axios");
const config = require("../configs/config.js");
// const inputDocs = require("../../models/inputDocs.js");
// http://${config.serverIp}:${config.serverPort}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
const blockExec = false;
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

(async function __main() {
    console.log("resetIdInInputDocs starting .....");
    if (blockExec) {
        console.log("blockExec");
        return 0;
    }
    let i = 0;
    const startDate = new Date();
    for (;;) {
        try {
            const reqData = {
                cond: {
                    nextTaskName: "resetIdInInputDocs",
                    // requestType: "parts class acquisition",
                    // requestType: "car modification acqusition additional information",
                },
                limit: 20000,
            };
            const res = await axios({
                // 1
                method: "post",
                url: `http://${config.serverIp}:${config.serverPort}/inputdocs/gettotreat`,
                data: reqData,
                headers: {
                    "Content-Type": "application/json",
                },
            });
            console.log("startDate", startDate);
            console.log("current date", new Date());
            const data = res.data;
            if (data.length === 0) {
                console.log("resetIdInInputDocs finishing .....");
                return;
            }
            for (let i in data) {
                const doc = data[i];
                const reqId = doc.payload.carModificationId;
                console.log(reqId);
                
                const child = await axios({
                    method: "post",
                    url: `http://${config.serverIp}:${config.serverPort}/inputdocs/searchone`,
                    data: {
                        cond: {
                            _id:reqId,
                        },
                        limit: 100,
                    },
                    headers: {
                        "Content-Type": "application/json",
                    },
                });
                // console.log(child)
                let newTaks = 'checkIfChildExist';
                const newId = child.data.payload.link;
                const res2 = await axios({
                    method: "patch",
                    url: `http://${config.serverIp}:${config.serverPort}/inputdocs/${doc._id}`,
                    data: { nextTaskName: newTaks, requestId: newId },
                    headers: {
                        "Content-Type": "application/json",
                    },
                });

                console.log(++i, res2.data);
            }
        } catch (e) {
            console.log(e);
        }
    }
})();

// });
