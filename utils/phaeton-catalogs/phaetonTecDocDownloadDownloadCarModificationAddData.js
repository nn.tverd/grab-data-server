// if there is no search button to click we shoul skip this item
// 96243577
const puppeteer = require("puppeteer");
const fetch = require("node-fetch");
const axios = require("axios");
const cheerio = require("cheerio");

const config = require("../configs/config.js");
// http://${config.serverIp}:${config.serverPort}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
const blockExec = true;
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
async function getCarModifications() {
    try {
        const response = await axios({
            method: "post",
            url: `http://${config.serverIp}:${config.serverPort}/inputdocs/search`,
            data: {
                cond: { requestType: "car modification acqusition additional information" },
                limit: 100000,
            },
            headers: {
                "Content-Type": "application/json",
            },
        });
        console.log(response.data.length);
        return response.data;
        // return new Promise((resolve) => {
        //     resolve(response.data);
        // });
    } catch (err) {
        console.log(err);
        return [];
    }
}
//-------------------------------------------------------------------------------
// const sleep = require("util").promisify(setTimeout)(
function timeout(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

const start_url = "http://catalog.phaeton.kz";

async function onPuppeteer() {
    const carModifications = await getCarModifications();
    console.log(carModifications.length);

    const browser = await puppeteer.launch({
        headless: false,
        devtools: false,
    });
    const page = await browser.newPage();

    await page.setViewport({
        width: 1920,
        height: 1080,
    });

    const carModificationCount = carModifications.length;
    const revCarModificationCount = 1 / carModificationCount;
    const startDate = new Date();
    for (let i in carModifications) {
        if (i % 100 == 0) {
            console.log(startDate);
            console.log(new Date());
            console.log(i, carModificationCount, i * revCarModificationCount);
        }
        const carModification = carModifications[i];
        const requestData = {
            sourse: start_url,
            requestId: carModification.payload.name,
            requestType: "car modification acqusition",
        };

        await page.goto(
            start_url + carModification.payload.additionalInfoClick,
            {
                waitUntil: "networkidle2",
            }
        );
        // await page.waitForTimeout(100);
        // return;
        const addData = await page.evaluate(() => {
            // const div_loadpage = document.querySelector(`#loadpage`);
            // const table_class = div_loadpage.querySelector(
            //     `table[class="table2wt content_table"]`
            // );
            // const table = table_class.querySelector(`table`);
            const table = document.querySelector(`table[cellpadding="5"]`);
            console.log(table);
            const trs = table.querySelectorAll("tr");

            // return [];
            const data = [];
            for (let i in trs) {
                if (i < 1) continue;
                const payload = {};
                const tr = trs[i];
                console.log(tr.innerText);
                if (!tr.querySelector) continue;
                // console.log(tr.innerText);

                const tds = tr.querySelectorAll(`td`);
                console.log("in browser", "step 1");
                payload.keyName = tds[0].textContent;
                payload.keyValue = tds[1].textContent;

                console.log("payload", payload);
                data.push(payload);
                // break;
            }
            console.log(data);
            return data;
        });

        console.log(i, addData.length);
        // return
        // await page.waitForTimeout(1000);
        // await page.click(`toclickid${i}`);
        // await page.waitForTimeout(1000);

        const newModification = {
            ...carModification,
        };
        // console.log(newModification);

        const id = newModification._id;
        newModification.treatedByAlgoriths.push(
            "phaeton-catalogs/phaetonTecDocDownloadDownloadCarModificationAddData.js"
        );
        newModification.requestType =
            "car modification acqusition additional information";
        newModification.payload.addData = [...addData];
        delete newModification._id;
        delete newModification.date;

        // console.log(newModification);
        try {
            const response = await axios({
                method: "patch",
                url: `http://${config.serverIp}:${config.serverPort}/inputdocs/${id}`,
                data: newModification,
                headers: {
                    "Content-Type": "application/json",
                },
            });
            console.log(response.data);
        } catch (err) {
            console.log(err);
        }
        // break;
    }
}
async function onFetch() {
    const carModifications = await getCarModifications();
    console.log(carModifications.length);

    const carModificationCount = carModifications.length;
    const revCarModificationCount = 1 / carModificationCount;
    const startDate = new Date();

    for (let i in carModifications) {
        if (i % 100 == 0) {
            console.log(startDate);
            console.log(new Date());
            console.log(i, carModificationCount, i * revCarModificationCount);
        }
        const carModification = carModifications[i];
        const { treatedByAlgoriths } = carModification;
        let alreadyTreated = false;
        // console.log( treatedByAlgoriths );
        for (let it in treatedByAlgoriths) {
            if (
                treatedByAlgoriths[it] ==
                "phaeton-catalogs/phaetonTecDocDownloadDownloadCarModificationAddData.js"
            )
                alreadyTreated = true;
        }
        if (alreadyTreated) continue;
        // console.log(start_url + carModification.payload.additionalInfoClick);
        try {
            const res = await fetch(
                start_url + carModification.payload.additionalInfoClick
            );
            const text = await res.text();

            // console.log(text);
            const $ = cheerio.load(text);
            const table = $('table[cellpadding="5"]');
            const list = $(table)
                .find("tr")
                .toArray()
                .map((element) => {
                    return $(element)
                        .find("td")
                        .toArray()
                        .map((tdEl) => $(tdEl).text());
                });
            // console.log(list);
            // console.log(list);
            for (let i in list) {
                if (list[i].length == 0) continue;
                const obj = {
                    keyName: list[i][0],
                    keyValue: list[i][1],
                };
                list[i] = obj;
            }

            // console.log(list);
            const addData = [...list];
            console.log(i, addData.length);

            const newModification = {
                ...carModification,
            };
            const id = newModification._id;
            newModification.treatedByAlgoriths.push(
                "phaeton-catalogs/phaetonTecDocDownloadDownloadCarModificationAddData.js"
            );

            newModification.requestType =
                "car modification acqusition additional information";
            newModification.payload.addData = [...addData];
            delete newModification._id;
            delete newModification.date;
            // console.log(newModification);
            // return;
            const response = await axios({
                method: "patch",
                url: `http://${config.serverIp}:${config.serverPort}/inputdocs/${id}`,
                data: newModification,
                headers: {
                    "Content-Type": "application/json",
                },
            });

            console.log(response.data);
        } catch (err) {
            console.log(err);
        }

        // break;
    }
}

(async () => {
    if (blockExec) {
        console.log("blockExec");
        return 0;
    }
    console.log("phaetonTecDocDownloadDownloadCarModificationAddData.....");
    await onFetch();
    // await onPuppeteer();
})();
