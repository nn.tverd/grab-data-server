// if there is no search button to click we shoul skip this item
// 96243577
const puppeteer = require("puppeteer");
const fetch = require("node-fetch");
const axios = require("axios");
const cheerio = require("cheerio");

const config = require("../configs/config.js");
// http://${config.serverIp}:${config.serverPort}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
const blockExec = true;
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
const params = {
    startUrl: "http://catalog.phaeton.kz",
    requestType: "parts class acquisition",
    requestTypeToSearch: "car modification acqusition additional information",
    treatedByAlgoriths:
        "phaeton-catalogs/phaetonTecDocDownloadDownloadPartsClasses.js",
};
//-------------------------------------------------------------------------------
async function getCarModifications() {
    try {
        const response = await axios({
            method: "post",
            url: `http://${config.serverIp}:${config.serverPort}/inputdocs/search`,
            data: {
                cond: {
                    requestType: params.requestTypeToSearch,
                },
                limit: 100000,
                // limit: 1,
            },
            headers: {
                "Content-Type": "application/json",
            },
        });
        console.log(response.data.length);
        return response.data;
        // return new Promise((resolve) => {
        //     resolve(response.data);
        // });
    } catch (err) {
        console.log(err);
        return [];
    }
}
//-------------------------------------------------------------------------------
// const sleep = require("util").promisify(setTimeout)(
function timeout(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

const start_url = "http://catalog.phaeton.kz";

async function onPuppeteer() {
    const carModifications = await getCarModifications();
    console.log(carModifications.length);

    const browser = await puppeteer.launch({
        headless: false,
        devtools: false,
    });
    console.log("reached step - 1");
    const page = await browser.newPage();
    console.log("reached step - 2");

    await page.setViewport({
        width: 1920,
        height: 1080,
    });
    console.log("reached step - 3");

    const carModificationCount = carModifications.length;
    const revCarModificationCount = 1 / carModificationCount;
    const startDate = new Date();
    let i = 0;
    console.log("reached step - 4");

    for (; i < carModifications.length; i++) {
        if (i < 43300) continue;
        if (i % 100 == 0) {
            console.log(startDate);
            console.log(new Date());
            console.log(i, carModificationCount, i * revCarModificationCount);
        }
        try {
            const carModification = carModifications[i];
            const requestData = {
                sourse: params.startUrl,
                requestId: carModification.payload.name,
                requestType: params.requestType,
            };

            await page.goto(start_url + carModification.payload.link, {
                waitUntil: "networkidle2",
            });
            // click on describe all
            // await page.goto1(123);
            await page.evaluate(() => {
                const as = document.querySelectorAll(`a`);
                for (let iA in as) {
                    const a = as[iA];
                    if (!a.getAttribute) continue;
                    const onclick = a.getAttribute("onclick");
                    console.log("onclick", onclick);
                    if (onclick.includes("describe_all")) {
                        a.click();
                        return;
                    }
                }
            });
            // await page.waitForTimeout(100);
            // return;
            const addData = await page.evaluate(() => {
                const data = [];
                const div_tree = document.querySelector("#tree_div");
                const nodes = div_tree.querySelectorAll(`div.dtreeNode`);
                console.log(nodes.length);
                let oneObject = {
                    maxlevel: 0,
                };

                for (let iN in nodes) {
                    // if (i > 30) break;
                    const node = nodes[iN];
                    if (!node.querySelector) continue;
                    const table = node.querySelector(`table`);
                    // console.log("table.innerText", table.innerText);
                    const tds = table.querySelectorAll(`td`);
                    const level = tds.length;
                    // if (level < oneObject.maxlevel) {
                    //     console.log("push one object", i, level, oneObject);

                    //     data.push(JSON.parse(JSON.stringify(oneObject)));
                    //     oneObject = {
                    //         maxlevel: 0,
                    //     };
                    // }
                    let keys = Object.keys(oneObject);
                    console.log(oneObject, keys);
                    for (let k in keys) {
                        const key = keys[k];
                        if (key == "maxlevel") continue;
                        // console.log(k, key, oneObject[key]);
                        const innerEnt = oneObject[key];
                        if (!innerEnt) continue;
                        // console.log(k, key, innerEnt, Object.keys(innerEnt));
                        console.log(innerEnt.level, level, oneObject);
                        if (!innerEnt.level) continue;
                        if (innerEnt.level >= level) {
                            delete oneObject[key];
                            console.log("deletion", key, oneObject);
                            continue;
                        }
                    }

                    const td = tds[level - 1];
                    const aTd = td.querySelector("a");
                    const link = aTd.href;
                    const text = aTd.innerText;
                    const name = aTd.getAttribute(`name`);
                    oneObject[`node-${iN}-level-${level}`] = {
                        level: level,
                        name: name,
                        link: link,
                        text: text,
                    };
                    // console.log("update one object", i, level, oneObject);
                    oneObject.maxlevel = level;
                    if (link.includes(`st=50`)) {
                        oneObject = JSON.parse(JSON.stringify(oneObject));
                        // console.log("push one object", i, level, oneObject);

                        data.push(JSON.parse(JSON.stringify(oneObject)));
                    }
                    // console.log(table.innerText, tds.length);
                }
                console.log(data);
                return data;
            });

            console.log(i, addData.length);
            // return;
            // return
            // await page.waitForTimeout(1000);
            // await page.click(`toclickid${i}`);
            // await page.waitForTimeout(1000);

            for (let d in addData) {
                /*
                    const params = {
                        startUrl: "http://catalog.phaeton.kz",
                        requestType: "parts class acquisition",
                        requestTypeToSearch: "car modification acqusition additional information",
                        treatedByAlgoriths:
                            "phaeton-catalogs/phaetonTecDocDownloadDownloadPartsClasses.js",
                    };
                */
                const newPartsClasses = {
                    ...requestData,
                    treatedByAlgoriths: [params.treatedByAlgoriths],
                    payload: {
                        ...addData[d],
                        carModificationId: carModification._id,
                    },
                };
                // console.log(newPartsClasses);
                // continue;
                try {
                    const response = await axios({
                        method: "post",
                        url: `http://${config.serverIp}:${config.serverPort}/inputdocs`,
                        data: newPartsClasses,
                        headers: {
                            "Content-Type": "application/json",
                        },
                    });
                    // console.log(response.data);
                } catch (err) {
                    console.log(err);
                }
            }
            // break;
        } catch (err) {
            console.log(i, "err", err);
            console.log(i);
            i -= 1;
            console.log(i);
            continue;
        }
    }
}
async function onFetch() {
    const carModifications = await getCarModifications();
    console.log(carModifications.length);

    const carModificationCount = carModifications.length;
    const revCarModificationCount = 1 / carModificationCount;
    const startDate = new Date();

    for (let i in carModifications) {
        if (i % 100 == 0) {
            console.log(startDate);
            console.log(new Date());
            console.log(i, carModificationCount, i * revCarModificationCount);
        }
        const carModification = carModifications[i];
        const { treatedByAlgoriths } = carModification;
        let alreadyTreated = false;
        // console.log( treatedByAlgoriths );
        for (let it in treatedByAlgoriths) {
            if (
                treatedByAlgoriths[it] ==
                "phaeton-catalogs/phaetonTecDocDownloadDownloadCarModificationAddData.js"
            )
                alreadyTreated = true;
        }
        if (alreadyTreated) continue;
        // console.log(start_url + carModification.payload.additionalInfoClick);
        try {
            const res = await fetch(
                start_url + carModification.payload.additionalInfoClick
            );
            const text = await res.text();

            // console.log(text);
            const $ = cheerio.load(text);
            const table = $('table[cellpadding="5"]');
            const list = $(table)
                .find("tr")
                .toArray()
                .map((element) => {
                    return $(element)
                        .find("td")
                        .toArray()
                        .map((tdEl) => $(tdEl).text());
                });
            // console.log(list);
            // console.log(list);
            for (let i in list) {
                if (list[i].length == 0) continue;
                const obj = {
                    keyName: list[i][0],
                    keyValue: list[i][1],
                };
                list[i] = obj;
            }

            // console.log(list);
            const addData = [...list];
            console.log(i, addData.length);

            const newModification = {
                ...carModification,
            };
            const id = newModification._id;
            newModification.treatedByAlgoriths.push(
                "phaeton-catalogs/phaetonTecDocDownloadDownloadCarModificationAddData.js"
            );

            newModification.requestType =
                "car modification acqusition additional information";
            newModification.payload.addData = [...addData];
            delete newModification._id;
            delete newModification.date;
            // console.log(newModification);
            // return;
            const response = await axios({
                method: "patch",
                url: `http://${config.serverIp}:${config.serverPort}/inputdocs/${id}`,
                data: newModification,
                headers: {
                    "Content-Type": "application/json",
                },
            });

            console.log(response.data);
        } catch (err) {
            console.log(err);
        }

        // break;
    }
}

(async () => {
    if (blockExec) {
        console.log("blockExec");
        return 0;
    }
    console.log(`${params.treatedByAlgoriths}.....`);
    // await onFetch();
    await onPuppeteer();
})();
