// if there is no search button to click we shoul skip this item
// 96243577
const puppeteer = require("puppeteer");
const fs = require("fs");
const axios = require("axios");

const config = require("../configs/config.js");
// http://${config.serverIp}:${config.serverPort}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
const blockExec = true;
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
async function getCarBrands() {
    try {
        const response = await axios({
            method: "post",
            url: `http://${config.serverIp}:${config.serverPort}`,
            data: {
                cond: { requestType: "car make acqusition" },
                limit: 10000,
            },
            headers: {
                "Content-Type": "application/json",
            },
        });
        console.log(response.data.length);
        return response.data;
        // return new Promise((resolve) => {
        //     resolve(response.data);
        // });
    } catch (err) {
        console.log(err);
        return [];
    }
}
//-------------------------------------------------------------------------------
// const sleep = require("util").promisify(setTimeout)(
function timeout(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

const start_url = "http://catalog.phaeton.kz";

(async () => {
    if (blockExec) {
        console.log("blockExec");
        return 0;
    }
    console.log("phaetonTecDocDownloadDownloadCarModels.....");
    const startDate = new Date();

    const carBrands = await getCarBrands();
    console.log(carBrands.length);

    const browser = await puppeteer.launch({
        headless: false,
        devtools: false,
    });

    const page = await browser.newPage();

    await page.setViewport({
        width: 1920,
        height: 10800,
    });
    for (let i in carBrands) {
        const carBrand = carBrands[i];
        const requestData = {
            sourse: start_url,
            requestId: carBrand.payload.name,
            requestType: "car model acqusition",
            algorithmName:
                "phaeton-catalogs/phaetonTecDocDownloadDownloadCarModels.js",
            treatedByAlgoriths: [
                "phaeton-catalogs/phaetonTecDocDownloadDownloadCarModels.js",
            ],
        };

        await page.goto(start_url + carBrand.payload.link, {
            waitUntil: "networkidle2",
        });
        await page.waitForTimeout(500);
        const models = await page.evaluate(() => {
            // const div_loadpage = document.querySelector(`#loadpage`);
            // const table_class = div_loadpage.querySelector(
            //     `table[class="table2wt content_table"]`
            // );
            // const table = table_class.querySelector(`table`);
            const trs = document.querySelectorAll(`tr.over`);
            console.log(trs.length);
            // return [];
            const data = [];
            for (let i in trs) {
                if (i === 0) continue;
                const payload = {};
                const tr = trs[i];
                console.log(tr.innerText);
                if (!tr.querySelector) continue;
                console.log(tr.innerText);

                const tds = tr.querySelectorAll(`td`);
                console.log("in browser", "step 1");
                for (let j in tds) {
                    console.log("in browser", "step 2", j, i);

                    const td = tds[j];
                    console.log("in browser", "step 2", j, i, td);

                    if (j == 0) {
                        console.log("in browser", "step 3", j, i);
                        const a = td.querySelector(`a`);
                        payload.name = a.innerText;
                        payload.link = a.getAttribute("href");
                    }
                    if (j == 1) {
                        console.log("in browser", "step 4", j, i);
                        payload.yearFrom = td.innerText;
                    }
                    if (j == 2) {
                        console.log("in browser", "step 5", j, i);
                        payload.yearTo = td.innerText;
                    }
                }
                console.log("payload", payload);
                data.push(payload);
            }
            return data;
        });
        console.log(models);
        // return;
        for (let i in models) {
            models[i].makeName = carBrand.payload.name;
            models[i].makeId = carBrand._id;
            const item = {
                ...requestData,
                payload: {
                    ...models[i],
                },

            };
            try {
                const response = await axios({
                    method: "post",
                    url: `http://${config.serverIp}:${config.serverPort}/inputdocs`,
                    data: item,
                    headers: {
                        "Content-Type": "application/json",
                    },
                });
                console.log(response.data);
            } catch (err) {
                console.log(err);
            }
            // break;
        }
        // break;
    }
})();
