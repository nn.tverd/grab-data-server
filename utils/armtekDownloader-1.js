// Only first data load to database
// 96243577
const puppeteer = require("puppeteer");
const fs = require("fs");
const axios = require("axios");

const config = require("./configs/config.js");
const cookie = require("./configs/armtekCookie.json");
//-------------------------------------------------------------------------------
async function getParts() {
    try {
        const response = await axios({
            method: "get",
            url: `http://${config.serverIp}:${config.serverPort}/part`,
            headers: {
                "Content-Type": "application/json",
            },
        });
        return response.data;
    } catch (err) {
        console.log(err);
        return [];
    }
}
async function getInputDocs() {
    try {
        const response = await axios({
            method: "get",
            url: `http://${config.serverIp}:${config.serverPort}/inputdocs`,
            headers: {
                "Content-Type": "application/json",
            },
        });
        return response.data;
    } catch (err) {
        console.log(err);
        return [];
    }
}

async function checkIfRequestHasBeenAlreadyExecuted(requestData) {
    try {
        const response = await axios({
            method: "post",
            url: `http://${config.serverIp}:${config.serverPort}/inputDocs/search`,
            data: {
                cond: requestData,
                limit: 1,
            },

            headers: {
                "Content-Type": "application/json",
            },
        });
        return response.data;
    } catch (err) {
        console.log(err);
        return [];
    }
}

//-------------------------------------------------------------------------------
// const sleep = require("util").promisify(setTimeout)(
function timeout(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
//-------------------------------------------------------------------------------
async function autoScroll(page) {
    await page.evaluate(async () => {
        await new Promise((resolve, reject) => {
            var totalHeight = 0;
            var distance = 100;
            var timer = setInterval(() => {
                var scrollHeight = document.body.scrollHeight;
                window.scrollBy(0, distance);
                totalHeight += distance;

                if (totalHeight >= scrollHeight) {
                    clearInterval(timer);
                    resolve();
                }
            }, 100);
        });
    });
}
//-------------------------------------------------------------------------------
async function parseFIRSTDATA(page) {
    console.log("parseFIRSTDATA starting.....");
    return await page.evaluate(() => {
        console.log("parseFIRSTDATA evaluating starts.....");
        let isItFIRSTDATA = true;
        let table = document.getElementById("component-search-table-FIRSTDATA");
        console.log("parseFIRSTDATA evaluating step 1 .....", table);
        if (table === null) {
            isItFIRSTDATA = false;
            table = document.getElementById("component-search-table-SRCDATA");
            if (table === null) {
                return new Promise((resolve, reject) => {
                    return resolve(null);
                });
            }
        }
        // FIRSTDATA
        const rTable = {
            type: "FIRSTDATA",
            payload: [],
        };
        console.log("parseFIRSTDATA evaluating step 2 .....");

        const trs = table.querySelectorAll("tr");
        console.log("parseFIRSTDATA evaluating step 3 .....");

        for (let r = 2; r < trs.length; r++) {
            console.log("parseFIRSTDATA evaluating step 2 for .....");
            const row = {};
            const tr = trs[r];
            const tds = tr.querySelectorAll("td");
            if (tds.length < 3) continue;

            row.armtekBrandName = tr.querySelector(
                "[class='brand-name']"
            ).innerText;
            row.armtekPartNumber = tr.querySelector(
                "[class='pin-name']"
            ).innerText;
            row.armtekName = tr.querySelector("[class='name-text']").innerText;
            if (isItFIRSTDATA) {
                const armtekRel_ = tr.querySelector(
                    "a[class='second_search cell-price-analog']"
                );
                console.log(armtekRel_);
                if (armtekRel_) {
                    row.armtekRel = armtekRel_.getAttribute("rel");
                } else {
                    row.armtekRel = null;
                }
                row.armtekRel = tr
                    .querySelector("a[class='second_search cell-price-analog']")
                    .getAttribute("rel");
            }
            row.isItFIRSTDATA = isItFIRSTDATA;
            row.articleInfo = tr
                .querySelector("[class='articleInfo']")
                .getAttribute("href");

            // name-text
            rTable.payload.push(row);
            // TODO: сделать промисы

            // return rTable;
        }
        return new Promise((resolve, reject) => {
            return resolve(rTable);
        });
    });
}
//-------------------------------------------------------------------------------
async function parseSRCDATA(page, rel) {
    console.log("async function parseSRCDATA(page) {", rel);
    // await page.waitForNavigation({ waitUntil: "networkidle0" });
    try {
        return await page.evaluate(() => {
            console.log("parseSRCDATA evaluation started .... ");
            table = document.getElementById("component-search-table-SRCDATA");
            //SRCDATA
            console.log("parseSRCDATA evaluation step 1 .... ");

            if (table === null) {
                // return null;
                return new Promise((resolve, reject) => {
                    return resolve(null);
                });
            }

            console.log("parseSRCDATA evaluation step 2 .... ");

            // debugger;
            // parse all the data on the page

            const trs = table.querySelectorAll("tr");
            console.log("parseSRCDATA evaluation step 3 .... ");

            const partsEntities = {};
            for (let t in trs) {
                console.log("parseSRCDATA evaluation step for - 1 .... ");

                const tr = trs[t];

                console.log(t, tr.innerText);
                if (!tr.querySelectorAll) continue;
                // debugger;
                const tds = tr.querySelectorAll("td");
                if (tds.length < 3) continue;
                // articleInfo
                articleInfo = tr
                    .querySelector("[class='articleInfo']")
                    .getAttribute("href");

                if (!partsEntities[articleInfo]) {
                    partsEntities[articleInfo] = { armtekNames: [] };
                }
                partsEntities[articleInfo].armtekArticleInfo = articleInfo;
                partsEntities[articleInfo].armtekBrandName = tr.querySelector(
                    "[class='brand-name']"
                ).innerText;
                if (partsEntities[articleInfo].armtekBrandName === "GM")
                    partsEntities[articleInfo].armtekBrandName =
                        "GENERAL MOTORS";
                partsEntities[articleInfo].armtekPartNumber = tr.querySelector(
                    "[class='pin-name']"
                ).innerText;
                const armtekName = tr.querySelector("[class='name-text']")
                    .innerText;
                partsEntities[articleInfo].armtekName = armtekName;
                partsEntities[articleInfo].armtekNames.push(armtekName);
                // partsEntities[
                //     articleInfo
                // ].armtekImageUrl = tr
                //     .querySelector("[data-imagelightbox='imageIcon']")
                //     .getAttribute("href");

                rTable = {
                    type: "SRCDATA",
                    payload: [],
                };
                for (let i in partsEntities) {
                    rTable.payload.push(partsEntities[i]);
                }
            }
            return new Promise((resolve, reject) => {
                return resolve(rTable);
            });
        });
    } catch (err) {
        console.log("catch(err){");
        console.log(err);

        return new Promise((resolve, reject) => {
            return reject(null);
        });
    }
}
//-------------------------------------------------------------------------------
async function isVisible(page, selector) {
    return await page.evaluate((selector) => {
        var e = document.querySelector(selector);
        if (e) {
            var style = window.getComputedStyle(e);

            return (
                style &&
                style.display !== "none" &&
                style.visibility !== "hidden" &&
                style.opacity !== "0"
            );
        } else {
            return false;
        }
    }, selector);
}
//-------------------------------------------------------------------------------
async function typeInSearch(page, partNumber) {
    await page.$eval("#query-search", (el) => (el.value = ""));
    await page.type("#query-search", `${partNumber}`, {
        delay: 30,
    });
    await page.click("#search-btn");
    await page.waitForSelector("#table-results-search-container");
    await page.waitForTimeout(1500);
    return page;
}
//-------------------------------------------------------------------------------
async function clickMoreArticles(page) {
    if ((await page.$("#more-articles")) !== null) {
        if (await isVisible(page, "#more-articles")) {
            console.log("found more-articles");
            await page.click("#more-articles");
            await page.waitForSelector("#table-results-search-container");
            await page.waitForTimeout(1500);
        }
    }
}
//-------------------------------------------------------------------------------
async function loginToArmtek(page) {
    if (Object.keys(cookie).length) {
        console.log(cookie[0].expires, new Date().getTime());
        console.log(new Date(cookie[0].expires), new Date());

        if (cookie[0].expires * 1000 > new Date().getTime()) {
            console.log("use cookie .... ");
            await page.goto(start_url, { waitUntil: "networkidle2" });
            await page.setCookie(...cookie);
            return null;
        }
    }
    console.log("login .... ");

    await page.goto(start_url, { waitUntil: "networkidle2" });
    await page.type("#login", config.armtekUser, { delay: 30 });
    await page.type("#password", config.armtekPassword, { delay: 30 });

    await page.click(
        "i[class='common-etp arm-icon arm-icon-checkbox-outline']"
    );
    await page.click("#login-btn");

    await page.waitForNavigation({ waitUntil: "networkidle0" });
    await page.waitForTimeout(1500);

    try {
        await page.waitForSelector("#query-search");
    } catch (error) {
        console.log(error);
        process.exit(0);
    }
    const currentCookie = await page.cookies();

    const cookieToWrite = [];
    for (let i in currentCookie) {
        if (currentCookie[i].name === "ci_sessions") {
            cookieToWrite.push(currentCookie[i]);
        }
    }

    console.log("currentCookie", currentCookie);
    fs.writeFileSync(
        "./utils/configs/armtekCookie.json",
        JSON.stringify(cookieToWrite)
    );
}
//-------------------------------------------------------------------------------
async function treatDataFromSRCDATA(searchResultsSRCDATA, part) {
    if (!searchResultsSRCDATA) {
        return new Promise((resolve, reject) => {
            resolve(null);
        });
    }
    if (searchResultsSRCDATA.type === "SRCDATA") {
        const armtekItems = searchResultsSRCDATA.payload;
        console.log(
            "armtekItems.length",
            armtekItems.length
            // armtekfd.armtekRel
        );

        for (let ai in armtekItems) {
            let item = armtekItems[ai];

            item.dataBasePartNumber = part.partNumber;
            item.dataBaseName = part.name;
            item.dataBaseId = part.dataBaseId;
            item.analogsGroup = part.analogsGroup;
            item.partSections = part.partSections;
            item.catalogs = part.catalogs;

            // catalogs catalogs
            // console.log(item);
            try {
                const response = await axios({
                    method: "post",
                    url: `http://${config.serverIp}:${config.serverPort}/part/write/armtek1`,
                    data: item,
                    headers: {
                        "Content-Type": "application/json",
                    },
                });
                return response.data;
            } catch (err) {
                console.log(err);
                return [];
            }
        }
    }
}
//-------------------------------------------------------------------------------
async function isLimitAchieved(page) {
    // modal-dialog
    return await page.evaluate(() => {
        const modal = document.querySelector("div[class='modal-dialog']");

        if (modal) {
            if (modal.innerText.toLowerCase().indexOf("закрыть")) {
                return new Promise((resolve) => {
                    resolve(true);
                });
            }
        }
        console.log(modal);
        return new Promise((resolve) => {
            resolve(false);
        });
    });
}
//-------------------------------------------------------------------------------

const start_url = "https://etp.armtek.eu/";
const start_url2 = "https://etp.armtek.eu";

(async () => {
    console.log("armtekDownloader.....");
    const startDate = new Date();

    const requestData = {
        sourse: "etp.armtek.eu",
        requestType: "analogCodeAquisition",
        algorithmName: "armtekDownloader-1",
    };

    // const parts = await getParts();
    const inputDocs = await getInputDocs();

    // console.log(parts[1]);

    // puppeteer.launch({devtools: true})
    const browser = await puppeteer.launch({
        headless: false,
        devtools: false,
    });

    const page = await browser.newPage();

    await page.setViewport({
        width: 1920,
        height: 10800,
    });

    await loginToArmtek(page);
    const startFrom = 0;

    console.log("inputDocs", inputDocs.length);

    for (let i in inputDocs) {
        if (i < startFrom) continue;
        const inputDoc = inputDocs[i];
        console.log(i, inputDoc.sourse, requestData.sourse);
        if (inputDoc.sourse !== requestData.sourse) continue;

        if (inputDoc.payload) {
            if (inputDoc.payload.array) {
                const { array } = inputDoc.payload;
                for (let ind in array) {
                    if (array[ind] === requestData.requestType) continue;
                }
            }
        }
        const entities = inputDoc.payload.array;
        for (let j in entities) {
            const entity = entities[j];
            const url = start_url2 + entity.articleInfo;
            console.log(entity, url);
            if (!entity.articleInfo) continue;

            await page.goto(start_url2 + entity.articleInfo, {
                waitUntil: "networkidle2",
            });
            const isLimit = await isLimitAchieved(page);
            if (isLimit) {
                await page.click(`[data-dismiss="modal"]`);
            }
            const data = await page.evaluate(() => {
                const parameters = {};

                function getParameters(el, parameters) {
                    const content = el.querySelector(
                        "div.content-container-body"
                    );
                    console.log("content", content);

                    const values = content.querySelector(
                        "div.content-part-values"
                    );
                    console.log("values", values);
                    // item-value
                    const items = values.querySelectorAll(
                        "span[class='item-value']"
                    );
                    console.log("items", items);
                    for (let i in items) {
                        const item = items[i];
                        console.log("item.innerText", item.innerText);
                        if (!item.innerText) continue;
                        if (item.innerText.indexOf("KA") > -1)
                            parameters.analogCode = item.innerText;
                        if (
                            item.innerText.indexOf("_") > -1 ||
                            item.innerText.indexOf("AN") > -1
                        )
                            parameters.armtekCode = item.innerText;
                    }
                }

                // const weightSize = document.getElementById("weight_size");
                // const dimensions = document.getElementById("dimensions");
                const additionally = document.getElementById("additionally");
                getParameters(additionally, parameters);

                return parameters;
            });
            entity.analogCode = data.analogCode;
            entity.armtekCode = data.armtekCode;
        }

        console.log("entities", entities);
        inputDoc.treatedByAlgoriths.push(requestData.algorithmName);

        try {
            const response = await axios({
                method: "patch",
                url: `http://${config.serverIp}:${config.serverPort}/inputdocs/${inputDoc._id}`,
                data: {
                    ...inputDoc,
                },
                headers: {
                    "Content-Type": "application/json",
                },
            });
            // return response.data;
        } catch (err) {
            console.log(err);
            // return [];
        }
        break;
    }
    // await browser.close();
})();
