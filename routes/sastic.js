const express = require("express");
const router = express.Router();
const Sastic = require("../models/sastic");

Sastic.createMapping(function (err, mapping) {
    if (err) {
        console.log("error creating mapping!");
        console.log(err);
    } else {
        console.log("mapping is created!");
        console.log(mapping);
    }
});

const stream = Sastic.synchronize();
let count = 0;

stream.on("data", function () {
    count++;
});
stream.on("close", function () {
    console.log(`indexed ${count} documnets`);
});
stream.on("error", function (err) {
    console.log(err);
});

router.get("/", async (req, res) => {
    try {
        const sastics = await Sastic.find();
        res.json(sastics);
    } catch (err) {
        res.json({ message: err });
    }
});
router.get("/:word", async (req, res) => {
    const word = req.params.word;
    console.log(word);
    try {
        const sastics = await Sastic.search(
            {
                query_string: {
                    query: word,
                },
            },
            // { hydrate: true },

            function (err, results) {
                if (err) {
                    console.log("search error", err);
                } else {
                    res.json(results);
                }
            }
        );
    } catch (err) {
        res.json({ message: err });
    }
});

router.post("/update", async (req, res) => {
    console.log("sastic update");
    try {
        const sastics = await Sastic.findOneAndUpdate(
            {
                _id: "5f9e6f1e3ecb653d7549db54",
            },
            {
                $push: { sub: { comment: "third comment", commentTag: "tag" } },
            },
            {
                new: true,
            }
        );
        res.json(sastics);
    } catch (err) {
        res.json({ message: err });
    }
});

module.exports = router;
