const express = require("express");
const inputDocs = require("../models/inputDocs.js");
const router = express.Router();
const InputDoc = require("../models/inputDocs.js");

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
const blockManyOperations = false;
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------

// common functions
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
function updateInputDocs(_id, nextTask ){

}
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

router.get("/", async (req, res) => {
    try {
        const fdList = await InputDoc.find();
        res.json(fdList);
    } catch (err) {
        res.json({ message: err });
    }
});
router.get("/count", async (req, res) => {
    try {
        const fdListCount = await InputDoc.find().count();
        res.json(fdListCount);
    } catch (err) {
        res.json({ message: err });
    }
});
router.get("/sample/:sample", async (req, res) => {
    const smpl = Number(req.params.sample);
    console.log(req.params.sample, smpl);
    try {
        const foundDocs = await InputDoc.aggregate([
            { $sample: { size: smpl } },
        ]);
        res.json(foundDocs);
    } catch (err) {
        console.log(err);
        res.json({ message: err });
    }
});
router.get("/updatemany/:operation/:code", async (req, res) => {
    if (blockManyOperations) {
        console.log("many operations are blocked");
        res.json({ message: "many operations are blocked" });
        return 0;
    }
    const _code = req.params.code;
    const operation = req.params.operation;

    const code = Number(_code);
    if (code !== 324875987456) {
        res.json({ message: "code is broken - " + code });
        return 0;
    }
    if (operation === "updateflag") {
        try {
            // const reqType = "car model acqusition";
            // const value = "init";
            const updatingResults = await InputDoc.updateMany(
                {},
                { $set: { isUnderTreatment: false, requestName: "" } }
            );
            console.log(updatingResults);
            res.json(updatingResults);
        } catch (err) {
            res.json({ message: err });
        }
    }
    if (operation === "changeidname") {
        console.log("changeidname start ..............");
        try {
            const count = await InputDoc.find().count();
            let i = 0;
            let j = 0;
            const step = 100000;
            let entities = [];
            for (;;) {
                if ((i - 1) * step + j >= count) {
                    console.log("changeidname finish ..............");
                    return res.json(count);
                }
                if (j % step === 0) {
                    console.log(
                        "changeidname getting chunk ..............",
                        i,
                        i / count
                    );
                    console.log(i, step * i, step);
                    entities = await InputDoc.find()
                        .skip(step * i)
                        .limit(step);
                    i += 1;
                    j = 0;
                    console.log(i, step * i, step, entities.length);
                }
                // console.log("changeidname one step 1 ..............", i, j, entities.length);
                const name = entities[j].requestId;
                const updatingResults = await InputDoc.updateOne(
                    { _id: entities[j]._id },
                    { $set: { requestName: name } }
                );

                if (updatingResults.ok) {
                    j += 1;
                }
            }
        } catch (err) {
            console.log(err);
            res.json({ message: err });
        }
    }
    if (operation === "insertid") {
        console.log("insertid start ..............");
        try {
            const count = await InputDoc.find().count();
            let i = 0;
            let j = 0;
            const step = 100;
            let entities = [];
            //-------------------------------------------------------------------------------//-------------------------------------------------------------------------------
            // const __requestType =
            //     "car model acqusition";
            // const __key = "makeId";
            //-------------------------------------------------------------------------------//-------------------------------------------------------------------------------
            // const __requestType =
            //     "car modification acqusition additional information";
            // const __key = "modelId";
            //-------------------------------------------------------------------------------//-------------------------------------------------------------------------------
            const __requestType = "parts class acquisition";
            const __key = "carModificationId";

            for (;;) {
                if ((i - 1) * step + j >= count) {
                    console.log("insertid finish ..............");
                    return res.json(count);
                }
                if (j % step === 0) {
                    console.log(
                        "insertid getting chunk ..............",
                        i,
                        (i * step + j) / count
                    );
                    console.log(i, step * i, step);
                    entities = await InputDoc.find({
                        requestType: __requestType,
                    })
                        .skip(step * i)
                        .limit(step);
                    i += 1;
                    j = 0;
                    console.log(i, step * i, step, entities.length);
                }
                console.log("entities[j]", j, entities.length);
                const sourceId = entities[j].payload[__key];

                if (!sourceId)
                    throw "what a hell? const sourceId = entities[j].payload[key]; is undefined";
                const source = await InputDoc.findOne({ _id: sourceId });
                // console.log(source);
                if (!source)
                    throw "what a hell? const source = await InputDoc.findOne({ _id: sourceId  }); is null";
                console.log("source", source);
                // return;
                const updatingResults = await InputDoc.updateOne(
                    { _id: entities[j]._id },
                    { $set: { requestId: source.payload.link } }
                );

                if (updatingResults.ok) {
                    j += 1;
                }
            }
        } catch (err) {
            console.log(err);
            res.json({ message: err });
        }
    }
    if (operation === "setNextTask") {
        console.log("insertid start ..............");
        try {
            const count = await InputDoc.find().count();
            let i = 0;
            let j = 0;
            const step = 100;
            let entities = [];
            //-------------------------------------------------------------------------------//-------------------------------------------------------------------------------
            // const __requestType =
            //     "car model acqusition";
            // const __key = "makeId";
            //-------------------------------------------------------------------------------//-------------------------------------------------------------------------------
            // const __requestType =
            //     "car modification acqusition additional information";
            // const __key = "modelId";
            //-------------------------------------------------------------------------------//-------------------------------------------------------------------------------
            const __requestType = "parts class acquisition";
            const __key = "carModificationId";

            for (;;) {
                if ((i - 1) * step + j >= count) {
                    console.log("insertid finish ..............");
                    return res.json(count);
                }
                if (j % step === 0) {
                    console.log(
                        "insertid getting chunk ..............",
                        i,
                        (i * step + j) / count
                    );
                    console.log(i, step * i, step);
                    entities = await InputDoc.find({
                        requestType: __requestType,
                    })
                        .skip(step * i)
                        .limit(step);
                    i += 1;
                    j = 0;
                    console.log(i, step * i, step, entities.length);
                }
                console.log("entities[j]", j, entities.length);
                const sourceId = entities[j].payload[__key];

                if (!sourceId)
                    throw "what a hell? const sourceId = entities[j].payload[key]; is undefined";
                const source = await InputDoc.findOne({ _id: sourceId });
                // console.log(source);
                if (!source)
                    throw "what a hell? const source = await InputDoc.findOne({ _id: sourceId  }); is null";
                console.log("source", source);
                // return;
                const updatingResults = await InputDoc.updateOne(
                    { _id: entities[j]._id },
                    { $set: { requestId: source.payload.link } }
                );

                if (updatingResults.ok) {
                    j += 1;
                }
            }
        } catch (err) {
            console.log(err);
            res.json({ message: err });
        }
    }
});
router.get("/clearundtreat", async (req, res) => {
    console.log("clearundtreat starting ........ ");
    const docsUnderTreatment = await inputDocs.find({ isUnderTreatment: true });
    console.log(docsUnderTreatment);
    const ids = docsUnderTreatment.map((item) => {
        return item._id;
    });
    console.log(ids);
    await inputDocs.updateMany(
        { _id: { $in: ids } },
        { $set: { isUnderTreatment: false } }
    );
    console.log(ids.length);
    res.json(ids.length);
});
router.get("/clearundtreat2", async (req, res) => {
    console.log("/clearundtreat2 .......");
    // console.log(req);

    const resp = await inputDocs.updateMany(
        {},
        { $set: { isUnderTreatment: 0 } }
    );
    console.log(resp);
    res.json(resp);
});
router.get("/getById/:postId", async (req, res) => {
    try {
        const fdList = await InputDoc.findOne({ _id: req.params.postId });
        res.json(fdList);
    } catch (err) {
        console.log(err);
        res.json({ message: err });
    }
});

router.post("/search", async (req, res) => {
    const b = req.body;
    const cond = b.cond;
    const limit = b.limit;
    const sort = b.sort;
    console.log(req.body);
    try {
        let fdList = [];
        if (!sort) {
            fdList = await InputDoc.find(cond).limit(limit);
        } else {
            fdList = await InputDoc.find(cond).sort(sort).limit(limit);
        }
        console.log(fdList, fdList.length);
        res.json(fdList);
    } catch (err) {
        res.json({ message: err });
    }
});
router.post("/searchone", async (req, res) => {
    const b = req.body;
    const cond = b.cond;
    const limit = b.limit;
    const sort = b.sort;
    console.log(req.body);
    try {
        fdList = await InputDoc.findOne(cond);
        console.log(fdList);
        res.json(fdList);
    } catch (err) {
        res.json({ message: err });
    }
});
router.post("/getonerandom", async (req, res) => {
    const b = req.body;
    mainCollectionCondition = b.mainCollectionCondition;
    const cond = b.cond;
    const limit = b.limit;
    const sort = b.sort;
    console.log(req.body);
    try {
        const foundDocs = await InputDoc.aggregate([
            {
                $match: {
                    requestType:
                        "car modification acqusition additional information",
                },
            },
            { $sample: { size: 100 } },
        ]);
        console.log(foundDocs);
        const docsToReturn = [];
        for (let i in foundDocs) {
            console.log("searching for ", i);
            const fd = foundDocs[i];
            const checkedDocs = await InputDoc.find({
                requestId: fd.payload.name,
            }).limit(1);
            docsToReturn.push(checkedDocs.length);
        }

        console.log(foundDoc);
        res.json(docsToReturn);
        // let fdList = [];
        // if (!sort) {
        //     fdList = await InputDoc.find(cond).limit(limit);
        // } else {
        //     fdList = await InputDoc.find(cond).sort(sort).limit(limit);
        // }
        // console.log(fdList, fdList.length);
        // res.json(fdList);
    } catch (err) {
        res.json({ message: err });
    }
});
router.post("/", async (req, res) => {
    // console.log("router.post(/, async (req, res) => {", req.body);
    const b = req.body;
    const fdItem = new InputDoc({ ...b });

    try {
        const savedFdItem = await fdItem.save();
        // console.log(savedFdItem);
        // console.log("successfully added!")
        res.json(savedFdItem);
    } catch (err) {
        console.log("adding problem added!", err);

        res.json({ message: err });
    }
});
router.post("/gettotreat", async (req, res) => {
    const b = req.body;
    const cond = b.cond;
    const limit = b.limit;
    let lock = b.lock;
    if (!lock) lock = 10 * 60000;
    // lock = Number(req.params.count);
    // const count = Number(req.params.count);
    // console.log(count);
    try {
        const foundDocs = await InputDoc.find({
            ...cond,
            isUnderTreatment: { $lte: new Date().getTime() },
        }).limit(limit);
        if (!foundDocs.length) {
            res.json([]);
            return [];
        }
        const ids = foundDocs.map((item) => {
            return item._id;
        });

        console.log( new Date().getTime(), new Date().getTime() + lock )
        await inputDocs.updateMany(
            { _id: { $in: ids } },
            { $set: { isUnderTreatment: new Date().getTime() + lock } }
        );
        console.log(foundDocs.length);
        res.json(foundDocs);
    } catch (err) {
        console.log(err);
        res.json({ message: err });
    }
});
router.post("/updatetreatedalgo", async (req, res) => {
    // {
    //   "_id":"5f784cc7ce575e63e05c765c",
    //   "filename": "filename4",
    //   "task": "task"
    // }
    const b = req.body;
    console.log("updatetreatedalgo", b);
    const filename = b.filename;
    const task = b.task;
    const path = b.path;
    const _id = b._id;

    const obj = {};

    try {
        const foundDoc = await InputDoc.findOne({ _id: _id });
        if (!foundDoc) {
            throw "updatetreatedalgo doc not found";
        }
        let treatedByAlgorithms = foundDoc.treatedByAlgorithms;
        if (!treatedByAlgorithms) {
            treatedByAlgorithms = {};
        }

        treatedByAlgorithms[filename] = {
            filename: filename,
            path: path,
            task: task,
            date: new Date(),
        };

        const updRes = await InputDoc.updateOne(
            { _id: _id },
            {
                $set: { treatedByAlgorithms: { ...treatedByAlgorithms } },
            }
        );

        console.log(
            "=============================================\n",
            updRes,
            "=============================================\n",
            "=============================================\n",
            "\n\n\n\n\n\n\n"
        );
        res.json(updRes);
    } catch (err) {
        console.log(err);
        res.json({ message: err });
    }
});
router.post("/updatemany", async (req, res) => {
    // {
    //   "_id":"5f784cc7ce575e63e05c765c",
    //   "filename": "filename4",
    //   "task": "task"
    // }
    const b = req.body;
    console.log("updatemany", b);
    const objectList = b;
    for (let i in objectList) {
        const newObject = objectList[i];
        console.log(newObject._id);
        // console.log(newObject);

        try {
            const updRes = await InputDoc.updateOne(
                { _id: newObject._id },
                { ...newObject }
            );
            console.log(
                "=============================================\n",
                updRes,
                "=============================================\n",
                "=============================================\n",
                "\n\n"
            );
        } catch (err) {
            console.log(err);
            res.json({ message: err });
        }
    }
    console.log({ message: "update is ok" });

    res.json({ message: "update is ok" });
});
router.post("/getManyByIdList", async (req, res) => {
    //   "_id":"5f784cc7ce575e63e05c765c",
    //   "filename": "filename4",
    //   "task": "task"
    // }
    const b = req.body;
    console.log("getManyByIdList", b);
    const list = b;

    try {
        const results = await InputDoc.find({ _id: { $in: list } });
        console.log(
            "=============================================\n",
            results,
            "=============================================\n",
            "=============================================\n",
            "\n\n\n\n\n\n\n"
        );
        res.json(results);
    } catch (err) {
        console.log(err);
        res.json({ message: err });
    }
});

router.delete("/:id", async (req, res) => {
    console.log("InputDocs.router.delete");

    try {
        const removedFdItem = await InputDoc.deleteOne({ _id: req.params.id });

        console.log(removedFdItem);
        res.json(removedFdItem);
    } catch (err) {
        console.log(err);
        res.json({ message: err });
    }
});

router.patch("/:id", async (req, res) => {
    // console.log(req.body);
    console.log(req.params.id, req.params.id.length);
    try {
        const updatedFdItem = await InputDoc.updateOne(
            { _id: req.params.id },
            { $set: { ...req.body, isUnderTreatment: false } }
        );

        console.log("updatedFdItem", updatedFdItem);
        res.json(updatedFdItem);
    } catch (err) {
        console.log("err", err);
        res.json({ message: err });
    }
});



module.exports = router;
