const express = require("express");
const router = express.Router();
const Part = require("../models/parts");

router.get("/", async (req, res) => {
    try {
        const parts = await Part.find();
        res.json(parts);
    } catch (err) {
        res.json({ message: err });
    }
});

router.post("/search", async (req, res) => {
    const b = req.body;
    const cond = b.cond;
    const limit = b.limit;
    const sort = b.sort;
    console.log(req.body);
    try {
        let parts = [];
        if (!sort) {
            parts = await Part.find(cond).limit(limit);
        } else {
            parts = await Part.find(cond).sort(sort).limit(limit);
        }
        console.log(parts);
        res.json(parts);
    } catch (err) {
        res.json({ message: err });
    }
});

router.post("/", async (req, res) => {
    console.log("router.post(/, async (req, res) => {", req.body);
    const b = req.body;
    const part = new Part({
        dataBaseId: b.dataBaseId,
        name: b.name,
        partNumber: b.partNumber,
        partSections: b.partSections,
        catalogs: b.catalogs,
        analogsGroup: b.analogsGroup,
    });

    try {
        const savedPart = await part.save();
        res.json(savedPart);
    } catch (err) {
        res.json({ message: err });
    }
});
// adding crosses from armtek without tecknical data
router.post("/write/armtek1", async (req, res) => {
    // adding crosses from armtek without tecknical data
    // console.log("router.post(/write/armtek1, async (req, res) => {", req.body);
    console.log("router.post(/write/armtek1, async (req, res) => {");
    const b = req.body;
    const partsFromDB = await Part.find({
        brand: b.armtekBrandName,
        partNumber: b.armtekPartNumber,
    });
    
    let mustCreateNew = true;

    for (let i in partsFromDB) {
        const partFromDB = partsFromDB[i];
        console.log( b.armtekBrandName, b.armtekPartNumber, partFromDB.brand, partFromDB.partNumber )
        if (partFromDB) {
            // check if updete or create
            if (partFromDB.armtekArticleInfo) {
                if (b.armtekArticleInfo === partFromDB.armtekArticleInfo) {
                    // skip action
                    mustCreateNew = false;
                } 
            } else {
                //update
                try {
                    const updatedPart = await Part.updateOne(
                        { _id: partFromDB._id },
                        { $set: { ...b } }
                        );
                        
                        console.log("updatedPart", updatedPart);
                        console.log(partFromDB._id, "/write/armtek1 updating ....");
                    res.json(updatedPart);
                    return null;
                } catch (err) {
                    res.json({ message: err });
                    console.log(partFromDB._id, "/write/armtek1 error1 ....");

                    return null;
                }
            }
        }
    }
    // create new
    /**
     * armtekArticleInfo
     * armtekBrandName
     * armtekPartNumber
     */
    if (mustCreateNew) {
        const part = new Part({
            dataBaseId: b.dataBaseId,
            name: b.armtekName,
            partNumber: b.armtekPartNumber,
            partSections: b.partSections,
            catalogs: b.catalogs,
            analogsGroup: b.analogsGroup,
            entityNumber: partsFromDB.length + 1,

            brand: b.armtekBrandName,
            isOriginal: b.armtekBrandName === "GENERAL MOTORS" ? true : false,
            isCross: b.armtekBrandName === "GENERAL MOTORS" ? false : true,

            armtekNames: b.armtekNames,
            armtekName: b.armtekName,
            armtekPartNumber: b.armtekPartNumber,
            armtekBrandName: b.armtekBrandName,
            armtekArticleInfo: b.armtekArticleInfo,
        });

        try {
            const savedPart = await part.save();
            console.log(savedPart._id, "/write/armtek1 creating ....");

            res.json(savedPart);
            return null;
        } catch (err) {
            res.json({ message: err });
            console.log("/write/armtek1 error2 ....", err);

            return null;
        }
    }
    console.log("/write/armtek1 skipping ....");
    
    res.json({});

});

// router.get("/:postId", async (req, res) => {
//     try {
//         const parts = await Post.findById(req.params.postId);
//         res.json(parts);
//     } catch (err) {
//         res.json({ message: err });
//     }
// });

router.delete("/:partId", async (req, res) => {
    try {
        const removedPart = await Part.remove({ _id: req.params.partId });

        res.json(removedPart);
    } catch (err) {
        res.json({ message: err });
    }
});

router.patch("/:partId", async (req, res) => {
    console.log(req.body);
    console.log(req.params.partId);
    try {
        const updatedPart = await Part.updateOne(
            { _id: req.params.partId },
            { $set: { ...req.body } }
        );

        console.log("updatedPart", updatedPart);
        res.json(updatedPart);
    } catch (err) {
        res.json({ message: err });
    }
});

module.exports = router;
