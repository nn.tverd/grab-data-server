const express = require("express");
const router = express.Router();
const Part = require("../models/fparts");

// **********************************************************************************************

router.get("/", async (req, res) => {
    try {
        const parts = await Part.find();
        res.json(parts);
    } catch (err) {
        res.json({ message: err });
    }
});

// **********************************************************************************************

router.post("/search", async (req, res) => {
    const b = req.body;
    const cond = b.cond;
    const limit = b.limit;
    const sort = b.sort;
    console.log(req.body);
    try {
        let parts = [];
        if (!sort) {
            parts = await Part.find(cond).limit(limit);
        } else {
            parts = await Part.find(cond).sort(sort).limit(limit);
        }
        console.log(parts);
        res.json(parts);
    } catch (err) {
        res.json({ message: err });
    }
});

// **********************************************************************************************

router.post("/", async (req, res) => {
    console.log("router.post(/, async (req, res) => {", req.body);
    const b = req.body;
    const part = new Part({
        dataBaseId: b.dataBaseId,
        name: b.name,
        partNumber: b.partNumber,
        partSections: b.partSections,
        catalogs: b.catalogs,
        analogsGroup: b.analogsGroup,
    });

    try {
        const savedPart = await part.save();
        res.json(savedPart);
    } catch (err) {
        res.json({ message: err });
    }
});

// **********************************************************************************************

router.post("/insertPart", async (req, res) => {
    // {
    //   constSec:{ bn: 1, pn:2, }
    //   payloadSec:{}
    // }
    const b = req.body;

    console.log("insertPart");
    const objectList = b;
    // for (let i in objectList) {
        // const cs = b.constSec;
        // const ps = b.payloadSec;
        const newObject = objectList;
        if (!newObject.payload || !newObject.payload.brandName) {
            // mark it as empry and skip insertion
        } else {
            const bn = newObject.payload.brandName.toUpperCase();
            const pn = newObject.payload.partNumber
                .replace(/[^a-zA-Zа-яА-Я0-9 ]/g, "")
                .toUpperCase();
            const name = newObject.payload.name;

            const upsertObj = {
                _id: {
                    bn: bn,
                    pn: pn,
                },
                bn: bn,
                pn: pn,
                pubNam: name,
            };
            console.log(newObject._id);
            // console.log(newObject);
            try {
                const upsRes = await Part.updateOne(
                    { _id: upsertObj._id },
                    { $set: { ...upsertObj } },
                    { upsert: true }
                );
                console.log(
                    "=============================================\n",
                    upsRes,
                    "\n=============================================",
                    "\n"
                );
                // const foundObject = await Part.findOne({
                //     _id: upsertObj._id,
                // });
                // console.log(foundObject);

                // const pld = foundObject.paylaod ? foundObject.paylaod : {};
                // pld[newObject._id] = { ...newObject };
                // console.log(pld);
                // const insertPayload = await Part.updateOne(
                //     { _id: upsertObj._id },
                //     { ...foundObject }
                // );
                // console.log(pld);

                // console.log("payload updated", insertPayload);
            } catch (err) {
                console.log(err);
                res.json({ message: err });
            }
        // }
    }
    console.log({ message: "update is ok" });

    res.json({ message: "update is ok" });
});

// **********************************************************************************************

// adding crosses from armtek without tecknical data
router.post("/gettotreat", async (req, res) => {
    const b = req.body;
    const cond = b.cond;
    const limit = b.limit;
    let lock = b.lock;
    if (!lock) lock = 10 * 60000;
    // lock = Number(req.params.count);
    // const count = Number(req.params.count);
    // console.log(count);
    try {
        const foundDocs = await Part.find({
            ...cond,
            isUnderTreatment: { $lte: new Date().getTime() },
        }).limit(limit);
        if (!foundDocs.length) {
            res.json([]);
            return [];
        }
        const ids = foundDocs.map((item) => {
            return item._id;
        });
        await Part.updateMany(
            { _id: { $in: ids } },
            { $set: { isUnderTreatment: new Date().getTime() + lock } }
        );
        console.log(foundDocs.length);
        res.json(foundDocs);
    } catch (err) {
        console.log(err);
        res.json({ message: err });
    }
});

// **********************************************************************************************

router.delete("/:partId", async (req, res) => {
    try {
        const removedPart = await Part.remove({ _id: req.params.partId });

        res.json(removedPart);
    } catch (err) {
        res.json({ message: err });
    }
});

// **********************************************************************************************

router.patch("/:partId", async (req, res) => {
    console.log(req.body);
    console.log(req.params.partId);
    try {
        const updatedPart = await Part.updateOne(
            { _id: req.params.partId },
            { $set: { ...req.body } }
        );

        console.log("updatedPart", updatedPart);
        res.json(updatedPart);
    } catch (err) {
        res.json({ message: err });
    }
});

module.exports = router;
