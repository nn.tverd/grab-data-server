const express = require("express");
const router = express.Router();
const Image = require("../models/imgFromGoogleSearch");

router.get("/", async (req, res) => {
    console.log("return all images");
    try {
        const images = await Image.find();
        res.json(images);
    } catch (err) {
        res.json({ message: err });
    }
});
router.post("/search", async (req, res) => {
    const b = req.body;
    console.log("searching for", b);
    const cond = b.cond;
    const limit = b.limit;
    console.log(b);
    try {
        const images = await Image.find(cond).limit(limit);
        res.json(images);
    } catch (err) {
        res.json({ message: err });
    }
});

router.post("/", async (req, res) => {
    console.log("router.post(/, async (req, res) => {", req.body);
    const b = req.body;
    const image = new Image({
        ...req.body,
    });

    try {
        const savedImage = await image.save();
        res.json(savedImage);
    } catch (err) {
        res.json({ message: err });
    }
});

// router.get("/:postId", async (req, res) => {
//     try {
//         const parts = await Post.findById(req.params.postId);
//         res.json(parts);
//     } catch (err) {
//         res.json({ message: err });
//     }
// });

router.delete("/:imageId", async (req, res) => {
    try {
        const removedImage = await Image.remove({ _id: req.params.imageId });

        res.json(removedImage);
    } catch (err) {
        res.json({ message: err });
    }
});

router.patch("/:imageId", async (req, res) => {
    try {
        const updatedImage = await Image.updateOne(
            { _id: req.params.imageId },
            { $set: { ...req.body } }
        );

        res.json(updatedImage);
    } catch (err) {
        res.json({ message: err });
    }
});

module.exports = router;
