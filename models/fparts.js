const mongoose = require("mongoose");

const FPartSchema = mongoose.Schema({
    _id: {
        bn: { type: String, required: true }, // brandName
        pn: { type: String, required: true }, // partNumber
    },
    // fixed data
    bn: { type: String, required: true }, // brandName
    pn: { type: String, required: true }, // partNumber
    isOrig: { type: Boolean, default: false }, // is part original (produced by car maker)? original = true, cross = false

    // normilezed data
    pubNam: { type: String, required: true }, // public name, can be different in one language, language depended

    // statistics and analytics
    entityNumber: { type: Number, default: 1 }, // use +1 if brand and partNumber already in database
    // system data
    dCreat: { type: Date, default: Date.now }, // date of creation documents
    dLasMod: { type: Date, default: Date.now }, // date of last modification

    // links peer to peer
    p2pL: { type: Object}, // { "part_id": score [0-100]} analog groups entry
    // links to parants (aggregators)
    parL: { type: Object}, // { "part_id": score [0-100]} cars, groups
    // links to children (images, texts)
    chL: { type: Object}, // { "part_id": score [0-100]} images, names, descriptions, inputDocs
    
    // -------------------------------------------------------------------------------------
    catalogs: { type: Array, required: true },
    // added by hands
    brand: { type: String },
    isCross: { type: Boolean, default: true },
    bestName: { type: String },
    synonyms: { type: Array },
    materials: { type: Array },
    characteristics: { type: Array },
    placeForUsage: { type: Array },

    // data from image treating
    popularityWithImage: { type: Number },
    googleSearchImgWasMade: { type: Boolean, default: false },
    googleSearchImgWasTreated: { type: Boolean, default: false },
    googleSearchImgTextAnalysisWasMade: { type: Boolean, default: false },
    mainImageSrc: { type: String },
    mainImageId: { type: String },
    mainImageFileName: { type: String },
    mainImageByResnet18: { type: String },
    topImagesByResnet18: { type: Array },
    vectorAlexnet: { type: Object },

    // text treatment from image and spacy
    spacyName3Words: { type: String },
    spacyName5Words: { type: String },
    spacyName7Words: { type: String },
    spacyName9Words: { type: String },
    spacyName11Words: { type: String },

    //

    // data from armtek
    armtekNames: { type: Array },
    armtekName: { type: String },
    armtekFPartNumber: { type: String },
    armtekBrandName: { type: String },
    armtekArticleInfo: { type: String },
});

module.exports = mongoose.model("FParts", FPartSchema);

// _id	name	partNumber	partSections	catalogs	analogsGroup
