const mongoose = require("mongoose");

const InputDocsSchema = mongoose.Schema({
    sourse: { type: String, required: true },       // where data came from. Example: "catalog.phaeton.kz", "PDF: drive.google.com?docId=123"
    requestId: { type: String, required: true },    // how we identify if request have treated or not
    requestName: { type: String, required: true },    // reason why request is made. Example: "init (if no initial entity)", "model-audi", "ps3033"
    requestType: { type: String, required: true },  // name of request
    payload: { type: Object },                      // acquired data
    treatedByAlgoriths: {type: Array},              // files names which were working on this data
    isUnderTreatment: {type: Number, default: false},              // if doc is under treatment by algorithm
    nextTaskName: { type: String, default: "" },                 // name of the next Task That should be applied to the doc
    treatedByAlgorithms: {type: Object},            // which files when where treated this algo

    date: {
        type: Date,
        default: Date.now,
    },
});

module.exports = mongoose.model("InputDocsSchema", InputDocsSchema);
