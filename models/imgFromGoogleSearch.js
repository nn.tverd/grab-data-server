const mongoose = require("mongoose");

const ImgFromGoogleSearch = mongoose.Schema({
    src: { type: String, required: true }, //link to image source
    alt: { type: String }, // text of the image
    title: { type: String, required: true }, // name under img on google search
    href: { type: String }, // link to the source page
    score: { type: Number, default: 50 }, // score set by human, higher score = stronger relation between picture and part number
    partNumberPretendToBe: {type: String}, // part number that was searched for on google 
    brandPretendToBe: {type: String}, // part brand that was searched for on google 
    partNumber: { type: Array }, // partnumber of detail on the picture
    brand: { type: Array }, // brand of the part on the picture
    analogsGroup: { type: String }, // analog group of the part
    wasTreatedByHuman: { type: Boolean, default: false }, // analog group of the part
    wasControlled: { type: Boolean, default: false }, // analog group of the part
    date: { type: Date, default: Date.now },
});

module.exports = mongoose.model("Image", ImgFromGoogleSearch);

// _id	name	partNumber	partSections	catalogs	analogsGroup
