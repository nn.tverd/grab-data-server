const mongoose = require("mongoose");

const PartSchema = mongoose.Schema({
    // from our database
    dataBaseId: { type: String, required: true },
    name: { type: String, required: true },
    partNumber: { type: String, required: true },
    catalogs: { type: Array, required: true },
    partSections: { type: Array, required: true },
    analogsGroup: { type: String, required: true },

    // added by hands
    brand: { type: String },
    isOriginal: { type: Boolean, default: false },
    isCross: { type: Boolean, default: true },
    bestName: { type: String },
    synonyms: { type: Array },
    materials: { type: Array },
    characteristics: { type: Array },
    placeForUsage: { type: Array },


    // system data
    date: { type: Date, default: Date.now },
    entityNumber:  { type: Number, default: 1 }, // use +1 if brand and partNumber already in database
    
    
    // data from image treating 
    popularityWithImage: { type: Number },
    googleSearchImgWasMade: { type: Boolean, default: false },
    googleSearchImgWasTreated: { type: Boolean, default: false },
    googleSearchImgTextAnalysisWasMade: { type: Boolean, default: false },
    mainImageSrc: { type: String },
    mainImageId: { type: String },
    mainImageFileName: { type: String },
    mainImageByResnet18: { type: String },
    topImagesByResnet18: {type: Array},
    vectorAlexnet: { type: Object },
    
    // text treatment from image and spacy
    spacyName3Words:{ type: String },
    spacyName5Words:{ type: String },
    spacyName7Words:{ type: String },
    spacyName9Words:{ type: String },
    spacyName11Words:{ type: String },
    
    // 

    // data from armtek
    armtekNames: {type: Array},
    armtekName: { type: String },
    armtekPartNumber:{ type: String },
    armtekBrandName: { type: String },
    armtekArticleInfo: { type: String },
});

module.exports = mongoose.model("Parts", PartSchema);

// _id	name	partNumber	partSections	catalogs	analogsGroup
