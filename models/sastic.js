const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const mongoosastic = require("mongoosastic");

const SubSacticSchema = {
    comment: { type: String },
    commentTag: { type: String },
};

const SasticSchema = Schema({
    title: { type: String, required: true, es_indexed: true },
    text2: { type: String, es_indexed: true },
    textNotInSearch: { type: String },
    sub: {
        type: [SubSacticSchema],
        es_indexed: true,
        es_type: "nested",
        es_include_in_parent: true,
    },

    date: {
        type: Date,
        default: Date.now,
    },
});

SasticSchema.plugin(mongoosastic, {
    index: "sastics",
    hosts: ["localhost:9200"],
    hydrate: true,
    hydrateOptions: { lean: true },
});

module.exports = mongoose.model("Sastics", SasticSchema);
