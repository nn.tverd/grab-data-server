const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

app.use(cors());

// bodyParser = {
//     json: { limit: "50mb", extended: true },
//     urlencoded: { limit: "50mb", extended: true },
// };
// app.use(bodyParser.json());
app.use(bodyParser.json({limit: '50mb', extended: true}))
// app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))

// --------------------------------------------------
const postRouters = require("./routes/posts");
app.use("/post", postRouters);
// --------------------------------------------------
const partRouters = require("./routes/parts");
app.use("/part", partRouters);
// --------------------------------------------------
const imageRouters = require("./routes/googleImages");
app.use("/gimage", imageRouters);
// --------------------------------------------------
const InputDocsRouters = require("./routes/inputDocs");
app.use("/inputdocs", InputDocsRouters);
// --------------------------------------------------
const FPartRouters = require("./routes/fparts");
app.use("/fparts", FPartRouters);
// --------------------------------------------------
const SasticRouters = require("./routes/sastic");
app.use("/sastic", SasticRouters);

mongoose.connect("mongodb://localhost:27017/grab-data-for-carcentre", () => {
    console.log("connected to db...");
});
/**
mongoose.Promise = Promise;
mongoose.set('useCreateIndex', true);
var mongooseOptions = {  useNewUrlParser: true }

mongoose.connect('mongodb://localhost:27017/MyDatabase', mongooseOptions, function(err) {
    if (err) {
        console.error('System could not connect to mongo server.')
        console.log(err)     
    } else {
        console.log('System connected to mongo server.')
    }
});
 */

// middlewares
// app.use("/post", () => {
//     console.log("this is middleware ");
// });

// routes
app.get("/", (req, res) => {
    res.send("we are on home");
});

// how to we start listening to the server
// comment added

app.listen(3031);
